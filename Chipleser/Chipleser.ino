#include <Arduino.h>
#include <SPI.h>
#include "myMFRC522.h"
 
#define SS_PIN D8
#define RST_PIN D1

MFRC522 mfrc522(SS_PIN, RST_PIN);
unsigned long cardId = 0;
 
void setup() {
  Serial.begin(115200);
  
   SPI.begin();
   mfrc522.PCD_Init();
 
  
}

char buffer[50];
uint8_t zeiger = 0;
unsigned long last_cardid = 0;
unsigned long last_send = 0;



void loop() {
  if (Serial.available() > 0)
  {
    while (Serial.available())
    {
      buffer[zeiger] = Serial.read();
      zeiger ++;
       if (zeiger > 10)
        {
          zeiger = 0;  //Speicherüberschreiber vermeiden
          
        }else if (buffer[zeiger -1] == '\n')
      {
        if (zeiger > 10)
        {
          zeiger = 0;  //Speicherüberschreiber vermeiden
   
        }
        if (buffer[0] == 'I')
        {if (buffer[1] == 'D')
        {if (buffer[2] == 'E')
        {if (buffer[3] == 'N')
        {if (buffer[4] == 'T')
        {Serial.println("Stuelpnerlauf");}}}}}
        zeiger = 0;
      }
      
    }
  }
  if (last_cardid != 0)
  {
    if ((last_send + 4000) < millis())
    {
      last_cardid = 0;
    }
    
  }
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  } 

  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }


  cardId = getCardId();

  if (last_cardid != cardId)
  {
     Serial.println(cardId);
     last_send = millis();
  }
  last_cardid = cardId;
 
  uint8_t control = 0x00;

  do {
    control = 0;
    for (int i = 0; i < 3; i++) {
      if (!mfrc522.PICC_IsNewCardPresent()) {
        if (mfrc522.PICC_ReadCardSerial()) {
          control |= 0x16;
        }

        if (mfrc522.PICC_ReadCardSerial()) {
          control |= 0x16;
        }

        control += 0x1;
      }

      control += 0x4;
    }

    delay(0);
  } while (control == 13 || control == 14);
 
   
  delay(10);

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
}

unsigned long getCardId() {
  byte readCard[4];
  for (int i = 0; i < 4; i++) {
    readCard[i] = mfrc522.uid.uidByte[i];
  }

  return (unsigned long)readCard[0] << 24
    | (unsigned long)readCard[1] << 16
    | (unsigned long)readCard[2] << 8
    | (unsigned long)readCard[3];
}
