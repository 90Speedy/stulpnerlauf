﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports Microsoft.VisualBasic

Public Class server

    Dim jsonimportedstarters As New List(Of JsonStarter)
    Dim counter As Integer = 0
    Dim comports As New List(Of IO.Ports.SerialPort)

    Dim SoundPlayer As New Media.SoundPlayer(My.Resources.beep)
    Private originalGroupBoxSize As Size





    Private Sub server_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MsgBox("Programm am Ende mit ALT + F4 schließen", MsgBoxStyle.OkOnly, "Hinweis")
        SoundPlayer.Load()
        neue_comports()
        ScaleGroupBoxToFitScreen()

        LabelVersion.Text = PVersion
    End Sub



    Private Sub ScaleGroupBoxToFitScreen()
        originalGroupBoxSize = GroupBox1.Size
        ' Holen der Bildschirmgröße
        Dim screenWidth As Integer = Screen.PrimaryScreen.Bounds.Width
        Dim screenHeight As Integer = Screen.PrimaryScreen.Bounds.Height

        ' Berechnen des Skalierungsfaktors basierend auf der Bildschirmgröße
        Dim scaleX As Double = screenWidth / originalGroupBoxSize.Width
        Dim scaleY As Double = screenHeight / originalGroupBoxSize.Height
        Dim scale As Double = Math.Min(scaleX, scaleY)

        ' Anwenden der Skalierung auf die GroupBox und ihre enthaltenen Steuerelemente
        GroupBox1.Scale(New SizeF(CSng(scale), CSng(scale)))

        ' Skalieren der Schriftgrößen der enthaltenen Labels
        For Each ctrl As Control In GroupBox1.Controls
            If TypeOf ctrl Is Label Then
                Dim lbl As Label = CType(ctrl, Label)
                lbl.Font = New Font(lbl.Font.FontFamily, lbl.Font.Size * CSng(scale), lbl.Font.Style)
            End If
        Next
        Me.WindowState = FormWindowState.Maximized
    End Sub



    Sub neue_comports()

        For Each port As System.IO.Ports.SerialPort In comports
            Try
                If port.IsOpen Then
                    port.Close()

                End If
            Catch ex As Exception

            End Try

        Next

        comports.Clear()

        Dim Lesegerätportliste As List(Of SerialPort_mitanzeige) = Search_comport_list()
        If Lesegerätportliste.Count > 0 Then
            For Each Lesegerätport In Lesegerätportliste
                Dim port As New System.IO.Ports.SerialPort(Lesegerätport.PortName, comgeschwindigkeit, IO.Ports.Parity.None, 8)
                port.PortName = Lesegerätport.PortName
                port.Open()
                comports.Add(port)

            Next

            TimerLesegeräte.Start()
        Else
            MsgBox("Fehler! Kein Lesegerät gefunden! Bitte anstecken und Programm neu starten!")
        End If







    End Sub



    Sub labelsclear()
        LabelGeburtsdatum.Text = ""
        LabelVorname.Text = ""
        LabelNachname.Text = ""
        LabelVerein.Text = ""
        LabelStartnummer.Text = ""
        LabelKategorie.Text = ""
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If counter = 0 Then
            labelsclear()
        End If
        If counter > 0 Then
            counter -= 1
        End If

        jsonimportedstarters.Clear()
        jsonimportedstarters = getjsonstartest(Nothing)



    End Sub

    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        'Bildschirmschoner verhindern
        My.Computer.Keyboard.SendKeys("{ESC}", True)
    End Sub

    Private Sub TimerLesegeräte_Tick(sender As Object, e As EventArgs) Handles TimerLesegeräte.Tick
        For Each port As System.IO.Ports.SerialPort In comports
            Try
                If port.IsOpen() Then
                    If port.BytesToRead > 0 Then
                        Dim tmp As String = port.ReadLine().Replace(vbCr, "").Replace(vbLf, "")

                        SoundPlayer.Play()
                        labelsclear()

                        senddatatolabels(tmp)



                    End If
                End If
            Catch ex As Exception
            End Try
        Next
    End Sub

    Private Sub server_SizeChanged(sender As Object, e As EventArgs) Handles MyBase.SizeChanged
        GroupBox1.Location = New Point(CInt(Me.ClientSize.Width / 2 - GroupBox1.Width / 2), CInt(Me.ClientSize.Height / 2 - GroupBox1.Height / 2))
    End Sub




    Sub senddatatolabels(chipnummer As String)
        Dim found As Boolean = False
        For Each starter In jsonimportedstarters
            If starter.chipnummer = chipnummer Then
                LabelKategorie.Text = starter.kategorie
                LabelGeburtsdatum.Text = starter.geburtsjahr

                LabelVorname.Text = starter.vorname
                LabelNachname.Text = starter.name
                LabelVerein.Text = starter.verein
                LabelStartnummer.Text = starter.startnummer
                found = True
            End If
        Next

        If Not found Then
            LabelGeburtsdatum.Text = "????"
            LabelVorname.Text = "????"
            LabelNachname.Text = "????"
            LabelVerein.Text = "????"
            LabelStartnummer.Text = "????"
            LabelKategorie.Text = "????"
        End If
        counter = 7


    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub
End Class
