/*
  ShiftRegister74HC595.cpp - Library for simplified control of 74HC595 shift registers.
  Created by Timo Denk (www.timodenk.com), Nov 2014.
  Additional information is available at http://shiftregister.simsso.de/
  Released into the public domain.
*/

#include "Arduino.h"
#include "my74HC595.h"


 
// ShiftRegister74HC595 constructor
ShiftRegister74HC595::ShiftRegister74HC595(int numberOfShiftRegisters, int serialDataPin1,int serialDataPin2,int serialDataPin3, int clockPin, int latchPin,int LA,int LB, int LC, int LD)
{
    // set attributes
    _numberOfShiftRegisters = numberOfShiftRegisters;

    _clockPin = clockPin;
    _serialDataPin1 = serialDataPin1;
    _serialDataPin2 = serialDataPin2;
    _serialDataPin3 = serialDataPin3;


  
    _LAPin = LA;
    _LBPin = LB;
    _LCPin = LC;
    _LDPin = LD;

    
    _latchPin = latchPin;
    pinMode(_LAPin, OUTPUT);
    pinMode(_LBPin, OUTPUT);
    pinMode(_LCPin, OUTPUT);
    pinMode(_LDPin, OUTPUT); 

    
    // define pins as outputs
    pinMode(clockPin, OUTPUT);
    pinMode(serialDataPin1, OUTPUT);
    pinMode(serialDataPin2, OUTPUT);
    pinMode(serialDataPin3, OUTPUT);
    pinMode(latchPin, OUTPUT);

    // set pins low
    digitalWrite(clockPin, LOW);
    digitalWrite(serialDataPin1, LOW);
    digitalWrite(serialDataPin2, LOW);
    digitalWrite(serialDataPin3, LOW);
    digitalWrite(latchPin, LOW);

    // allocates the specified number of bytes and initializes them to zero
    _digitalValues1 = (uint8_t *)malloc(numberOfShiftRegisters * sizeof(uint8_t));
    _digitalValues2 = (uint8_t *)malloc(numberOfShiftRegisters * sizeof(uint8_t));
    _digitalValues3 = (uint8_t *)malloc(numberOfShiftRegisters * sizeof(uint8_t));
    memset(_digitalValues1, 0, numberOfShiftRegisters * sizeof(uint8_t));
    memset(_digitalValues2, 0, numberOfShiftRegisters * sizeof(uint8_t));
    memset(_digitalValues3, 0, numberOfShiftRegisters * sizeof(uint8_t));

}


// ShiftRegister74HC595 destructor
// The memory allocated in the constructor is also released.
ShiftRegister74HC595::~ShiftRegister74HC595()
{
    free(_digitalValues1);
    free(_digitalValues2);
    free(_digitalValues3);
}

// Set all pins of the shift registers at once.
// digitalVAlues is a uint8_t array where the length is equal to the number of shift registers.
void ShiftRegister74HC595::setAll(const uint8_t * digitalValues1,const uint8_t * digitalValues2,const uint8_t * digitalValues3,uint8_t _zeile)
{
    memcpy( _digitalValues1, digitalValues1, _numberOfShiftRegisters);   // dest, src, size
    memcpy( _digitalValues2, digitalValues2, _numberOfShiftRegisters);   // dest, src, size
    memcpy( _digitalValues3, digitalValues3, _numberOfShiftRegisters);   // dest, src, size
    delayMicroseconds(500);

    
   
    for (int i = _numberOfShiftRegisters - 1; i >= 0; i--) {
        _shiftOut( _digitalValues1[i], _digitalValues2[i], _digitalValues3[i]);

    }
    digitalWrite(_latchPin, LOW);
    digitalWrite(_latchPin, HIGH);
     
    
     
    digitalWrite(_LAPin, ((_zeile & 1) > 0));
    digitalWrite(_LBPin, ((_zeile & 2) > 0));
    digitalWrite(_LCPin, ((_zeile & 4) > 0));
    digitalWrite(_LDPin, ((_zeile & 8) > 0));
    digitalWrite(_latchPin, LOW);
}

// Updates the shift register pins to the stored output values.
// This is the function that actually writes data into the shift registers of the 74HC595
 

 

 
void ShiftRegister74HC595::_shiftOut(uint8_t val1,uint8_t val2,uint8_t val3)
{
     uint8_t i;

     for (i = 0; i < 8; i++)  {
             
           digitalWrite(_serialDataPin1, !!(val1 & (1 << (7 - i))));
           digitalWrite(_serialDataPin2, !!(val2 & (1 << (7 - i))));
           digitalWrite(_serialDataPin3, !!(val3 & (1 << (7 - i))));
                 
           digitalWrite(_clockPin, HIGH);
           digitalWrite(_clockPin, LOW);            
     }
}
