/*
  ShiftRegister74HC595.h - Library for simplified control of 74HC595 shift registers.
  Created by Timo Denk (www.timodenk.com), Nov 2014.
  Additional information is available at http://shiftregister.simsso.de/
  Released into the public domain.
*/

#ifndef ShiftRegister74HC595_h
#define ShiftRegister74HC595_h

#include "Arduino.h"

class ShiftRegister74HC595 
{
public:
    
    ShiftRegister74HC595(int numberOfShiftRegisters, int serialDataPin1,int serialDataPin2,int serialDataPin3, int clockPin, int latchPin,int LA,int LB, int LC, int LD);
    ~ShiftRegister74HC595();
    void setAll(const uint8_t * digitalValues1,const uint8_t * digitalValues2,const uint8_t * digitalValues3,uint8_t _zeile);
    

   
    void updateRegisters();
   
   
private:
     
    void _shiftOut(uint8_t val1,uint8_t val2,uint8_t val3);

    int _numberOfShiftRegisters;
    int _clockPin;
    int _serialDataPin1;
    int _serialDataPin2;
    int _serialDataPin3;
    int _latchPin;
    uint8_t * _digitalValues1;
    uint8_t * _digitalValues2;
    uint8_t * _digitalValues3;

    int _LAPin;
    int _LBPin;
    int _LCPin;
    int _LDPin;


    
};

#endif
