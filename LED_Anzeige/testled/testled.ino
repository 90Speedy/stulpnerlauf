#include <ESP8266WiFi.h>
#include <espnow.h>
#include "my74HC595.h"

uint8_t pinValues1[16][12];
uint8_t pinValues2[16][12];
uint8_t pinValues3[16][12];

#define SHIFTNumberofregisters 12
#define SHIFTDataPin1  D4
#define SHIFTDataPin2  D7
#define SHIFTDataPin3  D8
#define SHIFTClockPin  D5
#define SHIFTLatchPin  D6

#define LAPin D0
#define LBPin D1
#define LCPin D2
#define LDPin D3

#define DEBUGENABLED 0 // Setzen Sie dies auf 0, um Debugging zu deaktivieren

#if DEBUGENABLED
  #define DebugPrint(x)  Serial.print(x)
  #define DebugPrintln(x)  Serial.println(x)
#else
  #define DebugPrint(x)
  #define DebugPrintln(x)
#endif

unsigned long biswann = 0; 
ShiftRegister74HC595 sr (SHIFTNumberofregisters, SHIFTDataPin1, SHIFTDataPin2, SHIFTDataPin3, SHIFTClockPin, SHIFTLatchPin, LAPin, LBPin, LCPin, LDPin);

const int bilddatenlaenge = 192;
typedef struct esp_package_anzeige {
  byte nummeranzeige;
  byte bilddaten[bilddatenlaenge];
  byte kennung1;
  byte kennung2;
  byte kennung3;
  byte dauer;  
} esp_package_anzeige;

esp_package_anzeige myData;

void OnDataRecv(unsigned char* mac, unsigned char* incomingData, unsigned char len) {
  memcpy(&myData, incomingData, sizeof(myData));
  DebugPrint("Data received: ");
  DebugPrintln(len);
  DebugPrint("kennung1: ");
  DebugPrintln(myData.kennung1);
  DebugPrint("kennung2: ");
  DebugPrintln(myData.kennung2);
  DebugPrint("kennung3: ");
  DebugPrintln(myData.kennung3);
  DebugPrint("nummeranzeige: ");
  DebugPrintln(myData.nummeranzeige);
  DebugPrint("bilddaten ");
  DebugPrintln((char*)myData.bilddaten);
  DebugPrint("dauer: ");
  DebugPrintln(myData.dauer);
  DebugPrintln();

  if (myData.dauer > 0) {
    biswann = millis() + (unsigned long) myData.dauer * 1000;
  } else {
    biswann = 0; // Dauerhaft an
  }

  if (myData.kennung1 != 31 || myData.kennung2 != 46 || myData.kennung3 != 175 || myData.nummeranzeige < 1 || myData.nummeranzeige > 3) return;

  DebugPrintln("Daten IO");
  DebugPrint("Empfangene Daten von serial: ");

#if DEBUGENABLED
  for (int i = 0; i < bilddatenlaenge; i++) {
      DebugPrint(myData.bilddaten[i]);
      DebugPrint(" ");
  }
  DebugPrintln("");
#endif
// Kopieren der Daten abhängig von der Anzeige Nummer
switch (myData.nummeranzeige) {
  case 1: memcpy(pinValues1[0], myData.bilddaten, bilddatenlaenge); break;
  case 2: memcpy(pinValues2[0], myData.bilddaten, bilddatenlaenge); break;
  case 3: memcpy(pinValues3[0], myData.bilddaten, bilddatenlaenge); break;
  default:DebugPrintln("Ungültige Nummeranzeige"); break;
}
  DebugPrintln("");
}

void setup() {
  Serial.begin(115200);
  DebugPrintln("");
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != 0) {
    DebugPrintln("Error initializing ESP-NOW");
  } else {
    DebugPrintln("IO initializing ESP-NOW");
  }
  esp_now_register_recv_cb(OnDataRecv);
  init_bilddaten();
}

void init_bilddaten() {
  memset(pinValues1, 0xFF, sizeof(pinValues1));
  memset(pinValues2, 0xFF, sizeof(pinValues2));
  memset(pinValues3, 0xFF, sizeof(pinValues3));
}

void loop() {
  if ((biswann != 0) && (biswann < millis())) {
    biswann = 0;
    DebugPrintln("ENDE");
    init_bilddaten();
  }
  for (uint8_t zeile = 0; zeile < 16; zeile++) {
    sr.setAll(pinValues1[zeile], pinValues2[zeile], pinValues3[zeile], zeile);
  }
}