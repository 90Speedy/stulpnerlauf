﻿Imports System.IO.Ports
Imports System.Net
Imports System.Net.Sockets

Public Class LED_Anzeige

    Const anzahl_bytes_nur_Bild As Integer = 192
    Const anzahl_bytes_mit_Kennungen As Integer = anzahl_bytes_nur_Bild + 5 'nummer 3x kennung, dauer, feld

    Dim systemfont As Font

    Dim MAXZEILE As Integer = 16
    Dim MAXSPALTE As Integer = 96


    Sub New()
        systemfont = New Font("schrift.ttf", 10)
    End Sub

    Public Sub sende(porte As List(Of SerialPort_mitanzeige), zeile1 As String, zeile2 As String, zeile3 As String, mittig1 As Boolean, mittig2 As Boolean, mittig3 As Boolean, dauerinsekunden As Byte)



        Try


            Dim bm1 As New Bitmap(96, 16)
            Dim bm2 As New Bitmap(96, 16)
            Dim bm3 As New Bitmap(96, 16)

            bm1 = textzuBild(zeile1, mittig1, systemfont)
            bm2 = textzuBild(zeile2, mittig2, systemfont)
            bm3 = textzuBild(zeile3, mittig3, systemfont)

            Dim sendebytes1(anzahl_bytes_mit_Kennungen) As Byte
            Dim sendebytes2(anzahl_bytes_mit_Kennungen) As Byte
            Dim sendebytes3(anzahl_bytes_mit_Kennungen) As Byte

            sendebytes1(0) = 31
            sendebytes1(1) = 46
            sendebytes2(0) = 31
            sendebytes2(1) = 46
            sendebytes3(0) = 31
            sendebytes3(1) = 46



            Array.Copy(bildzubytefeld(bm1), 0, sendebytes1, 2, anzahl_bytes_nur_Bild)
            Array.Copy(bildzubytefeld(bm2), 0, sendebytes2, 2, anzahl_bytes_nur_Bild)
            Array.Copy(bildzubytefeld(bm3), 0, sendebytes3, 2, anzahl_bytes_nur_Bild)

            sendebytes1(anzahl_bytes_mit_Kennungen - 1) = 175
            sendebytes2(anzahl_bytes_mit_Kennungen - 1) = 175
            sendebytes3(anzahl_bytes_mit_Kennungen - 1) = 175
            sendebytes1(anzahl_bytes_mit_Kennungen - 2) = dauerinsekunden
            sendebytes2(anzahl_bytes_mit_Kennungen - 2) = dauerinsekunden
            sendebytes3(anzahl_bytes_mit_Kennungen - 2) = dauerinsekunden

            sendebytes1(anzahl_bytes_mit_Kennungen - 3) = 1 'feldnummer
            sendebytes2(anzahl_bytes_mit_Kennungen - 3) = 2 'feldnummer
            sendebytes3(anzahl_bytes_mit_Kennungen - 3) = 3 'feldnummer


            Dim delay As Integer = 20
            For Each myport In porte
                If myport.AnzeigeAvailable Then
                    If myport.IsOpen Then
                        myport.Write(sendebytes1, 0, anzahl_bytes_mit_Kennungen)
                        Threading.Thread.Sleep(delay)
                        myport.Write(sendebytes2, 0, anzahl_bytes_mit_Kennungen)
                        Threading.Thread.Sleep(delay)
                        myport.Write(sendebytes3, 0, anzahl_bytes_mit_Kennungen)
                        'Threading.Thread.Sleep(500)
                    End If
                End If
            Next

        Catch ex As Exception

        End Try


    End Sub

    Private Function bildzubytefeld(mybitmap As Bitmap) As Byte()
        Dim sendebytes(anzahl_bytes_nur_Bild) As Byte
        For zeile As Integer = 0 To (MAXZEILE - 1)
            For spalte As Integer = 0 To (MAXSPALTE - 1)
                If mybitmap.GetPixel((MAXSPALTE - spalte - 1), zeile).GetBrightness < 0.5 Then
                    Dim bytenummer As Byte = (spalte + MAXSPALTE * zeile) \ 8
                    Dim bitnr As Byte = spalte Mod 8
                    Dim bitwert As Byte = 2 ^ bitnr
                    sendebytes(bytenummer) = sendebytes(bytenummer) + bitwert
                    'sendebytes(bytenummer) = bitwert
                End If
            Next
        Next
        'Hinweis, der Controller braucht das invertiert, früher habe ich das auf dem controller gemacht aber auf einem richigen rechner ist mehr rechenleistung
        For feld As Integer = 0 To anzahl_bytes_nur_Bild - 1
            sendebytes(feld) = Not sendebytes(feld)
        Next



        Return (sendebytes)
    End Function

    Private Function textzuBild(mytext As String, mittig As Boolean, myfont As Font) As Bitmap
        Dim mybm As New Bitmap(96, 16)
        Dim imgSize As System.Drawing.Size = TextRenderer.MeasureText(mytext, myfont)
        Dim imgWidth As Integer = 96
        Dim imgHeight As Integer = 16
        Dim startew As Integer = 0 ' (imgWidth - imgSize.Width) / 2
        Dim starteh As Integer = (imgHeight - imgSize.Height) / 2
        If mittig Then

            startew = ((imgWidth - imgSize.Width) / 2)
        Else
            startew = 0
        End If
        Using Graphics As Graphics = Graphics.FromImage(mybm)
            Graphics.FillRectangle(New SolidBrush(Color.White), 0, 0, imgWidth, imgHeight)
            Graphics.DrawString(mytext, myfont, New SolidBrush(Color.Black), startew, starteh)
        End Using
        Return mybm
    End Function

End Class
