﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AuswahlForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ButtonÜbernahme = New System.Windows.Forms.Button()
        Me.ButtonLäuferLöschen = New System.Windows.Forms.Button()
        Me.DataGridViewLog = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListViewDatenimport = New System.Windows.Forms.ListView()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DataGridViewLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButtonÜbernahme
        '
        Me.ButtonÜbernahme.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonÜbernahme.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonÜbernahme.Location = New System.Drawing.Point(619, 569)
        Me.ButtonÜbernahme.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonÜbernahme.Name = "ButtonÜbernahme"
        Me.ButtonÜbernahme.Size = New System.Drawing.Size(212, 49)
        Me.ButtonÜbernahme.TabIndex = 7
        Me.ButtonÜbernahme.Text = "Übernahme"
        Me.ButtonÜbernahme.UseVisualStyleBackColor = True
        '
        'ButtonLäuferLöschen
        '
        Me.ButtonLäuferLöschen.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonLäuferLöschen.Location = New System.Drawing.Point(4, 10)
        Me.ButtonLäuferLöschen.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonLäuferLöschen.Name = "ButtonLäuferLöschen"
        Me.ButtonLäuferLöschen.Size = New System.Drawing.Size(202, 48)
        Me.ButtonLäuferLöschen.TabIndex = 4
        Me.ButtonLäuferLöschen.Text = "Lösche alte Daten"
        Me.ButtonLäuferLöschen.UseVisualStyleBackColor = True
        '
        'DataGridViewLog
        '
        Me.DataGridViewLog.AllowUserToAddRows = False
        Me.DataGridViewLog.AllowUserToDeleteRows = False
        Me.DataGridViewLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridViewLog.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridViewLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.DataGridViewLog.Location = New System.Drawing.Point(4, 638)
        Me.DataGridViewLog.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.DataGridViewLog.Name = "DataGridViewLog"
        Me.DataGridViewLog.ReadOnly = True
        Me.DataGridViewLog.RowHeadersWidth = 51
        Me.DataGridViewLog.RowTemplate.Height = 24
        Me.DataGridViewLog.Size = New System.Drawing.Size(827, 228)
        Me.DataGridViewLog.TabIndex = 8
        '
        'Column1
        '
        DataGridViewCellStyle41.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle41
        Me.Column1.HeaderText = "Level"
        Me.Column1.MinimumWidth = 6
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 58
        '
        'Column2
        '
        DataGridViewCellStyle42.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle42
        Me.Column2.HeaderText = "Text"
        Me.Column2.MinimumWidth = 6
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 53
        '
        'Column3
        '
        DataGridViewCellStyle43.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle43
        Me.Column3.HeaderText = "Time"
        Me.Column3.MinimumWidth = 6
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 55
        '
        'Column4
        '
        DataGridViewCellStyle44.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle44
        Me.Column4.HeaderText = "Modul"
        Me.Column4.MinimumWidth = 6
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 61
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(193, 80)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Lauf markieren " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "mehrere mit STRG/SHIFT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "dann Start jetzt drücken" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'ListViewDatenimport
        '
        Me.ListViewDatenimport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListViewDatenimport.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewDatenimport.HideSelection = False
        Me.ListViewDatenimport.Location = New System.Drawing.Point(211, 50)
        Me.ListViewDatenimport.Name = "ListViewDatenimport"
        Me.ListViewDatenimport.Size = New System.Drawing.Size(397, 568)
        Me.ListViewDatenimport.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.ListViewDatenimport.TabIndex = 13
        Me.ListViewDatenimport.UseCompatibleStateImageBehavior = False
        Me.ListViewDatenimport.View = System.Windows.Forms.View.List
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(211, 10)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(397, 34)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Hole Infos aus Datenbank"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'AuswahlForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(842, 877)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListViewDatenimport)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DataGridViewLog)
        Me.Controls.Add(Me.ButtonÜbernahme)
        Me.Controls.Add(Me.ButtonLäuferLöschen)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MinimumSize = New System.Drawing.Size(858, 916)
        Me.Name = "AuswahlForm"
        Me.Text = "Läufer-Import aus Datenbank"
        CType(Me.DataGridViewLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ButtonÜbernahme As Button
    Friend WithEvents ButtonLäuferLöschen As Button
    Friend WithEvents DataGridViewLog As DataGridView
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Label2 As Label
    Friend WithEvents ListViewDatenimport As ListView
    Friend WithEvents Button1 As Button
End Class
