﻿Imports Lauf.FormStuelpnerlauf
Imports Newtonsoft.Json


Public Class AuswahlForm

    Dim jsonimportedstarters As New List(Of JsonStarter)

    Private Sub ButtonLäuferLöschen_Click(sender As Object, e As EventArgs) Handles ButtonLäuferLöschen.Click

        If (MsgBox("Alle Daten löschen. Bist du dir sicher?", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok) Then
            log_color(errorlevel.Fehler, "Daten werden gelöscht", errortype.EImport, DataGridViewLog)


            ListViewDatenimport.Items.Clear()

            Dim sicherungsordner As String = "altedaten" & DateTime.Now.ToString("yyyyMMddHHmmss")

            'starterordner
            'allecsvdateien
            Try
                ' Überprüfen, ob der Sicherungsordner existiert, wenn nicht, erstellen
                If Not System.IO.Directory.Exists(sicherungsordner) Then
                    System.IO.Directory.CreateDirectory(sicherungsordner)
                End If

                ' Verschieben des Starterordners in den Sicherungsordner


                If System.IO.Directory.Exists(starterordner) Then
                    System.IO.Directory.Move(starterordner, System.IO.Path.Combine(sicherungsordner, starterordner))
                    log_color(errorlevel.Hinweis, $"Der Ordner {starterordner} wurde nach {sicherungsordner} verschoben.", errortype.EImport, DataGridViewLog)
                Else
                    log_color(errorlevel.Fehler, $"Der Ordner {starterordner} existiert nicht.", errortype.EImport, DataGridViewLog)
                End If

                ' Verschieben aller CSV-Dateien im aktuellen Ordner in den Sicherungsordner
                Dim aktuelleOrdner As String = System.IO.Directory.GetCurrentDirectory()
                Dim csvDateien As String() = System.IO.Directory.GetFiles(aktuelleOrdner, "*.csv")

                For Each csvDatei As String In csvDateien
                    Dim zielDatei As String = System.IO.Path.Combine(sicherungsordner, System.IO.Path.GetFileName(csvDatei))
                    System.IO.File.Move(csvDatei, zielDatei)
                    log_color(errorlevel.Hinweis, $"Die Datei {csvDatei} wurde nach {zielDatei} verschoben.", errortype.EImport, DataGridViewLog)
                Next

            Catch ex As System.IO.IOException
                log_color(errorlevel.Fehler, $"Ein Fehler ist aufgetreten: {ex.Message}", errortype.EImport, DataGridViewLog)
            Catch ex As UnauthorizedAccessException
                log_color(errorlevel.Fehler, $"Zugriff verweigert: {ex.Message}", errortype.EImport, DataGridViewLog)
            Catch ex As Exception
                log_color(errorlevel.Fehler, $"Ein unerwarteter Fehler ist aufgetreten: {ex.Message}", errortype.EImport, DataGridViewLog)
            End Try

        End If
        neuinitialisieren()
    End Sub


    Private Sub ButtonÜbernahme_Click(sender As Object, e As EventArgs) Handles ButtonÜbernahme.Click

        System.IO.Directory.CreateDirectory(starterordner)
        For Each starter In jsonimportedstarters
            If ListViewDatenimport.SelectedItems.Cast(Of ListViewItem)().Any(Function(item) item.Text = starter.kategorie_plusSort) Then
                Dim pfad As String = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), starterordner, CInt(starter.startnummer).ToString("D3") & ".txt")
                Dim tmp As New laufer(pfad, starter.vorname, starter.name, starter.kategorie_plusSort, starter.verein, starter.chipnummer)
            End If
        Next

        jsonimportedstarters.Clear()
        ListViewDatenimport.Items.Clear()
        Me.Close()

    End Sub



    Sub neuinitialisieren()
        System.IO.Directory.CreateDirectory(starterordner)
        ListViewDatenimport.Items.Clear()
        jsonimportedstarters = getjsonstartest(DataGridViewLog)
        For Each starter In jsonimportedstarters
            starter.kategorie_plusSort = "(" & CInt(starter.reihenfolge).ToString("D2") & ") " & starter.kategorie
            If Not ListViewDatenimport.Items.Cast(Of ListViewItem)().Any(Function(item) item.Text = starter.kategorie_plusSort) Then
                ListViewDatenimport.Items.Add(starter.kategorie_plusSort)
            End If
        Next

    End Sub
    Private Sub AuswahlForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        neuinitialisieren()
    End Sub
End Class