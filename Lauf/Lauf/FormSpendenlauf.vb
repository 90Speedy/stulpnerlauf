﻿Imports System.ComponentModel

Public Class FormSpendenlauf
    ReadOnly starterordner As String = "Starter"

    Dim laeuferliste As New List(Of laufer_round)
    Dim startnummer_zu_laufer_liste As New Dictionary(Of Integer, laufer_round)()
    Dim chip_zu_laufer_liste As New Dictionary(Of ULong, laufer_round)()
    ReadOnly Ergebnis As String = "GesamtErgebnis.csv"
    Dim myanzeige As New LED_Anzeige()

    Dim SoundPlayer As New Media.SoundPlayer(My.Resources.beep)


    Dim comports As New List(Of SerialPort_mitanzeige)

    Private Sub vorbereitenfuersenden(vorn As String, name As String, runden As String)

        letzter_N = name
        letzte_V = vorn
        letzte_R = runden
        senderaus()
    End Sub

    Private Sub FormSpendenlauf_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SoundPlayer.Load()




        System.IO.Directory.CreateDirectory(starterordner)
        For Each file As IO.FileInfo In New System.IO.DirectoryInfo(starterordner).GetFiles("*.txt")
            Dim mylaufer As New laufer_round(file.FullName)
            laeuferliste.Add(mylaufer)
            startnummer_zu_laufer_liste.Add(mylaufer.get_nr_as_int, mylaufer)
            Dim chipnr As String = mylaufer.get_chipnr()
            If chipnr <> "" Then
                If IsNumeric(chipnr) Then
                    If Not (chip_zu_laufer_liste.ContainsKey(CULng(chipnr))) Then
                        chip_zu_laufer_liste.Add(CULng(chipnr), mylaufer)
                    Else
                        logge("Fehler! Doppelte Chipnummer!: " & mylaufer.get_nr_as_string)
                    End If
                Else
                    logge("Fehler! Komische Startnummer in: " & mylaufer.get_nr_as_string)
                End If
            Else
                logge("Fehler! Keine Chipnummer hinterlegt für Starter: " & mylaufer.get_nr_as_string)
            End If
        Next
        If (laeuferliste.Count = 0) Then
            MsgBox("Läuferordner ist leer. Bitte lege die Läuferfiles in folgenden Ordner und starte das Programm neu!")
        End If

        laeuferliste.Sort(Function(x, y) x.get_nr_as_int.CompareTo(y.get_nr_as_int))






        liste_aktualisieren()


        neue_comports()
        myanzeige.sende(comports, "Spendenlauf", "SV1870 e.V.", "", False, False, False, 0)

    End Sub

    Sub neue_comports()





        tryschliesse_alle_comports(comports)
        comports.Clear()



        Dim Lesegerätportliste As List(Of SerialPort_mitanzeige) = Search_comport_list()
        If Lesegerätportliste.Count > 0 Then
            For Each Lesegerätport In Lesegerätportliste
                logge("Lesegerät an " & Lesegerätport.PortName & " gefunden, mit Sender für Anzeige: " & Lesegerätport.AnzeigeAvailable)

                Dim port As New SerialPort_mitanzeige(Lesegerätport.PortName, comgeschwindigkeit, IO.Ports.Parity.None, 8)
                port.AnzeigeAvailable = Lesegerätport.AnzeigeAvailable
                port.PortName = Lesegerätport.PortName
                port.Open()
                comports.Add(port)
            Next
            TimerLesegeräte.Start()
        Else
            logge("Fehler! Kein Lesegerät gefunden! Bitte anstecken und Programm neu starten!")
        End If

    End Sub
    Sub liste_aktualisieren()
        For Each tmplaufer As laufer_round In laeuferliste
            Dim neu As Boolean = True
            For Each zeile As DataGridViewRow In DataGridView1.Rows

                If (zeile.Cells(0).Value = tmplaufer.get_nr_as_string) Then
                    zeile.Cells(2).Value = Convert.ToInt32(tmplaufer.get_runde)

                    neu = False
                End If

            Next


            If neu Then
                DataGridView1.Rows.Add(tmplaufer.get_nr_as_string, tmplaufer.get_nachname & ", " & tmplaufer.get_vorname, tmplaufer.get_runde)

            End If
        Next


        DataGridView1.AutoResizeColumn(0)
        DataGridView1.AutoResizeColumn(1)
        DataGridView1.AutoResizeColumn(2)

        DataGridView1.AutoResizeColumn(3)
        DataGridView1.AutoResizeColumn(4)
        DataGridView1.AutoResizeColumn(5)

    End Sub


    Private Sub DataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        If e.RowIndex >= 0 Then

            If e.ColumnIndex = 3 Then '+
                ' MsgBox(("Row : " + e.RowIndex.ToString & "  Col : ") + e.ColumnIndex.ToString)


                startnummer_zu_laufer_liste(CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)).set_runde_relative(1)
                liste_aktualisieren()






            End If
            If e.ColumnIndex = 4 Then '-
                'MsgBox(("Row : " + e.RowIndex.ToString & "  Col : ") + e.ColumnIndex.ToString)
                startnummer_zu_laufer_liste(CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)).set_runde_relative(-1)
                liste_aktualisieren()
            End If


            If e.ColumnIndex = 5 Then 'aktualisieren

            End If

            If e.ColumnIndex = 5 Or e.ColumnIndex = 4 Or e.ColumnIndex = 3 Then 'aktualisieren
                Dim nachname As String = startnummer_zu_laufer_liste(CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)).get_nachname
                Dim vorname As String = startnummer_zu_laufer_liste(CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)).get_vorname
                Dim runden As String = startnummer_zu_laufer_liste(CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)).get_runde
                ' Dim nummer As String = startnummer_zu_laufer_liste(CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)).get_nr_as_string
                vorbereitenfuersenden(vorname, nachname, runden)
            End If

        End If
    End Sub

    Sub logge(zeile As String)

        log_color(errorlevel.Hinweis, zeile, errortype.EStart, DataGridViewLog)

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles TimerLesegeräte.Tick
        Dim fehler As Boolean = False

        For Each port As System.IO.Ports.SerialPort In comports
            Try
                If port.IsOpen() Then
                    If port.BytesToRead > 0 Then
                        Dim tmp As String = port.ReadLine()
                        If IsNumeric(tmp) Then
                            If chip_zu_laufer_liste.ContainsKey(CULng(tmp)) Then

                                SoundPlayer.Play()
                                chip_zu_laufer_liste(CULng(tmp)).set_runde_relative(1)

                                Dim nachname As String = chip_zu_laufer_liste(CULng(tmp)).get_nachname
                                Dim vorname As String = chip_zu_laufer_liste(CULng(tmp)).get_vorname
                                Dim runden As String = chip_zu_laufer_liste(CULng(tmp)).get_runde
                                Dim nummer As String = chip_zu_laufer_liste(CULng(tmp)).get_nr_as_string


                                vorbereitenfuersenden(vorname, nachname, runden)

                                logge("Chip erkannt: " & nummer &
                                       " Runden: " & runden & " (" &
                                       nachname & ", " &
                                       vorname & ")"
                                       )



                                liste_aktualisieren()
                            Else
                                logge("Warnung! Unbekannte Chipnummer: " & tmp)
                            End If
                        Else
                            logge("Warnung! Lesegerät hat keine Zahl geschickt: " & tmp)
                        End If
                    End If
                Else
                    logge("Fehler! Lesegerätproblem, Stoppe Lesegerät:" & port.PortName)
                    fehler = True
                End If
            Catch ex As Exception
                logge("Fehler! Lesegerätproblem, Stoppe Lesegerät:" & port.PortName)
                fehler = True

            End Try

        Next
        If fehler Then
            neue_comports()
        End If


    End Sub

    Sub add_zeile_ergebnis(startnummer As String, nachname As String, vorname As String, rundenzahl As String, ergebnisdatei As String)
        Dim zeile As String = ""
        Dim trennzeichen As String = ";"
        zeile = startnummer & trennzeichen _
                 & nachname & trennzeichen _
                 & vorname & trennzeichen _
                 & rundenzahl & vbCrLf
        My.Computer.FileSystem.WriteAllText(ergebnisdatei, zeile, True)
    End Sub

    Private Sub ButtongenerateCSV_Click(sender As Object, e As EventArgs) Handles ButtongenerateCSV.Click

        If System.IO.File.Exists(Ergebnis) Then
            System.IO.File.Delete(Ergebnis)
        End If
        add_zeile_ergebnis("Nr", "Nachname", "Vorname", "Runden", Ergebnis)
        For Each laufer As laufer_round In laeuferliste
            add_zeile_ergebnis(laufer.get_nr_as_int, laufer.get_nachname, laufer.get_vorname, laufer.get_runde, Ergebnis)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        neue_comports()
    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Timerzeit.Start()
        eswarschonmalzeit = True
        endzeit = Date.Now.AddMinutes(NumericUpDownMinuten.Value)
    End Sub

    Dim endzeit As DateTime
    Dim letzter_N As String = ""
    Dim letzte_V As String = ""
    Dim letzte_R As String = ""
    Dim eswarschonmalzeit = False

    Private Sub Timerzeit_Tick(sender As Object, e As EventArgs) Handles Timerzeit.Tick
        senderaus()
    End Sub


    Sub senderaus()
        Dim mytimespan As TimeSpan
        mytimespan = DateTime.Now - endzeit
        Dim zeitdifstring As String
        If mytimespan.Ticks > 0 Then
            zeitdifstring = "00:00:00"
        Else
            zeitdifstring = mytimespan.ToString("hh\:mm\:ss")
        End If



        Dim zeile1 As String = letzte_V
        Dim zeile2 As String = letzter_N
        Dim zeile3 As String = ""
        If IsNumeric(letzte_R) Then
            Dim letzteRasint = Convert.ToInt32(letzte_R)
            zeile3 = letzteRasint.ToString
            If letzteRasint < 10 Then
                zeile3 = "  " + zeile3
            End If
            If letzteRasint < 100 Then
                zeile3 = "  " + zeile3
            End If
        Else
            zeile3 = letzte_R
        End If
        zeile3 = zeile3 + "      "


        If eswarschonmalzeit Then
            zeile3 = zeile3 + zeitdifstring
        End If



        LabelZ1.Text = zeile1
        LabelZ2.Text = zeile2
        LabelZ3.Text = zeile3
        myanzeige.sende(comports, zeile1, zeile2, zeile3, False, False, False, 15)
    End Sub

    Private Sub FormSpendenlauf_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing

        laeuferliste.Clear()
        startnummer_zu_laufer_liste.Clear()
        chip_zu_laufer_liste.Clear()
        tryschliesse_alle_comports(comports)
        TimerLesegeräte.Stop()
    End Sub
End Class
