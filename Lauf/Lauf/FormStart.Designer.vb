﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormStart
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonDatenpflege = New System.Windows.Forms.Button()
        Me.ButtonStülpnerlauf = New System.Windows.Forms.Button()
        Me.ButtonSpendenlauf = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ButtonDatenpflege
        '
        Me.ButtonDatenpflege.Location = New System.Drawing.Point(9, 10)
        Me.ButtonDatenpflege.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonDatenpflege.Name = "ButtonDatenpflege"
        Me.ButtonDatenpflege.Size = New System.Drawing.Size(151, 41)
        Me.ButtonDatenpflege.TabIndex = 0
        Me.ButtonDatenpflege.Text = "Datenpflege"
        Me.ButtonDatenpflege.UseVisualStyleBackColor = True
        '
        'ButtonStülpnerlauf
        '
        Me.ButtonStülpnerlauf.Location = New System.Drawing.Point(9, 55)
        Me.ButtonStülpnerlauf.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonStülpnerlauf.Name = "ButtonStülpnerlauf"
        Me.ButtonStülpnerlauf.Size = New System.Drawing.Size(151, 47)
        Me.ButtonStülpnerlauf.TabIndex = 1
        Me.ButtonStülpnerlauf.Text = "Stülpnerlauf"
        Me.ButtonStülpnerlauf.UseVisualStyleBackColor = True
        '
        'ButtonSpendenlauf
        '
        Me.ButtonSpendenlauf.Location = New System.Drawing.Point(9, 107)
        Me.ButtonSpendenlauf.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonSpendenlauf.Name = "ButtonSpendenlauf"
        Me.ButtonSpendenlauf.Size = New System.Drawing.Size(151, 46)
        Me.ButtonSpendenlauf.TabIndex = 2
        Me.ButtonSpendenlauf.Text = "Spendenlauf"
        Me.ButtonSpendenlauf.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(128, 197)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Vx.xx"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 159)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Bluetooth am PC ausschalten!"
        '
        'FormStart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(172, 219)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonSpendenlauf)
        Me.Controls.Add(Me.ButtonStülpnerlauf)
        Me.Controls.Add(Me.ButtonDatenpflege)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.Name = "FormStart"
        Me.Text = "SV1870 Laufsortware"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ButtonDatenpflege As Button
    Friend WithEvents ButtonStülpnerlauf As Button
    Friend WithEvents ButtonSpendenlauf As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
