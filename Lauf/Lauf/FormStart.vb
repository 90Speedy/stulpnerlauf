﻿Public Class FormStart
    Private Sub ButtonDatenpflege_Click(sender As Object, e As EventArgs) Handles ButtonDatenpflege.Click
        AuswahlForm.ShowDialog()
    End Sub

    Private Sub ButtonStülpnerlauf_Click(sender As Object, e As EventArgs) Handles ButtonStülpnerlauf.Click
        FormStuelpnerlauf.ShowDialog()
    End Sub

    Private Sub ButtonSpendenlauf_Click(sender As Object, e As EventArgs) Handles ButtonSpendenlauf.Click
        FormSpendenlauf.ShowDialog()
    End Sub

    Private Sub FormStart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label1.Text = PVersion
    End Sub
End Class