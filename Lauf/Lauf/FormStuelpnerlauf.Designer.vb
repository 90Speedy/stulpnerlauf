﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormStuelpnerlauf
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormStuelpnerlauf))
        Me.ButtonStart_FixZeit = New System.Windows.Forms.Button()
        Me.DateTimePickerZeitStart = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePickerDatumStart = New System.Windows.Forms.DateTimePicker()
        Me.ButtonStart_flexZeit = New System.Windows.Forms.Button()
        Me.ListViewStart = New System.Windows.Forms.ListView()
        Me.TabControlStart = New System.Windows.Forms.TabControl()
        Me.TabPageStart = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.NumericUpDownMsStart = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPageStop = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DateTimePickerDatumStop = New System.Windows.Forms.DateTimePicker()
        Me.NumericUpDownMsStop = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DateTimePickerZeitStop = New System.Windows.Forms.DateTimePicker()
        Me.ButtonFlexZeit = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ButtonDisquali = New System.Windows.Forms.Button()
        Me.ButtonZeit_Loeschen = New System.Windows.Forms.Button()
        Me.ButtonStopFixZeit = New System.Windows.Forms.Button()
        Me.ListViewStop = New System.Windows.Forms.ListView()
        Me.TabPageAuswertung = New System.Windows.Forms.TabPage()
        Me.ButtonLaufOK = New System.Windows.Forms.Button()
        Me.TextBoxLaufname = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ButtonAuswertung = New System.Windows.Forms.Button()
        Me.TabPageUrkunden = New System.Windows.Forms.TabPage()
        Me.GroupBoxUrkunden = New System.Windows.Forms.GroupBox()
        Me.CheckBoxTrosturkunde = New System.Windows.Forms.CheckBox()
        Me.GroupBoxFilterUrkunde = New System.Windows.Forms.GroupBox()
        Me.rB1_alleAK = New System.Windows.Forms.RadioButton()
        Me.CheckBoxUrkundeBis = New System.Windows.Forms.CheckBox()
        Me.ComboBoxFilterAK = New System.Windows.Forms.ComboBox()
        Me.NumericUpDownUrkundeVon = New System.Windows.Forms.NumericUpDown()
        Me.rB1_Statnr_von_b = New System.Windows.Forms.RadioButton()
        Me.NumericUpDownUrkundeBis = New System.Windows.Forms.NumericUpDown()
        Me.rB1_folgAK = New System.Windows.Forms.RadioButton()
        Me.ButtonUrkundendruck = New System.Windows.Forms.Button()
        Me.GroupBoxFilterStufe2Urkunde = New System.Windows.Forms.GroupBox()
        Me.NumericUpDownMaxPlatz = New System.Windows.Forms.NumericUpDown()
        Me.RB_Limit_Urku = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.TimerComport = New System.Windows.Forms.Timer(Me.components)
        Me.DataGridViewLog = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pdoc = New System.Drawing.Printing.PrintDocument()
        Me.Pprev = New System.Windows.Forms.PrintPreviewDialog()
        Me.Pprintd = New System.Windows.Forms.PrintDialog()
        Me.TabControlStart.SuspendLayout()
        Me.TabPageStart.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDownMsStart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageStop.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericUpDownMsStop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageAuswertung.SuspendLayout()
        Me.TabPageUrkunden.SuspendLayout()
        Me.GroupBoxUrkunden.SuspendLayout()
        Me.GroupBoxFilterUrkunde.SuspendLayout()
        CType(Me.NumericUpDownUrkundeVon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDownUrkundeBis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxFilterStufe2Urkunde.SuspendLayout()
        CType(Me.NumericUpDownMaxPlatz, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridViewLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButtonStart_FixZeit
        '
        Me.ButtonStart_FixZeit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonStart_FixZeit.BackColor = System.Drawing.Color.Lime
        Me.ButtonStart_FixZeit.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonStart_FixZeit.Location = New System.Drawing.Point(203, 314)
        Me.ButtonStart_FixZeit.Name = "ButtonStart_FixZeit"
        Me.ButtonStart_FixZeit.Size = New System.Drawing.Size(147, 83)
        Me.ButtonStart_FixZeit.TabIndex = 1
        Me.ButtonStart_FixZeit.Text = "Start Jetzt"
        Me.ButtonStart_FixZeit.UseVisualStyleBackColor = False
        '
        'DateTimePickerZeitStart
        '
        Me.DateTimePickerZeitStart.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePickerZeitStart.Location = New System.Drawing.Point(6, 45)
        Me.DateTimePickerZeitStart.MinDate = New Date(2018, 1, 1, 0, 0, 0, 0)
        Me.DateTimePickerZeitStart.Name = "DateTimePickerZeitStart"
        Me.DateTimePickerZeitStart.ShowUpDown = True
        Me.DateTimePickerZeitStart.Size = New System.Drawing.Size(117, 26)
        Me.DateTimePickerZeitStart.TabIndex = 3
        '
        'DateTimePickerDatumStart
        '
        Me.DateTimePickerDatumStart.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePickerDatumStart.CalendarMonthBackground = System.Drawing.Color.White
        Me.DateTimePickerDatumStart.Location = New System.Drawing.Point(6, 19)
        Me.DateTimePickerDatumStart.Name = "DateTimePickerDatumStart"
        Me.DateTimePickerDatumStart.Size = New System.Drawing.Size(242, 26)
        Me.DateTimePickerDatumStart.TabIndex = 5
        '
        'ButtonStart_flexZeit
        '
        Me.ButtonStart_flexZeit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonStart_flexZeit.Location = New System.Drawing.Point(254, 19)
        Me.ButtonStart_flexZeit.Name = "ButtonStart_flexZeit"
        Me.ButtonStart_flexZeit.Size = New System.Drawing.Size(115, 47)
        Me.ButtonStart_flexZeit.TabIndex = 6
        Me.ButtonStart_flexZeit.Text = "Ändere Startzeit mit eingestellter Zeit"
        Me.ButtonStart_flexZeit.UseVisualStyleBackColor = True
        '
        'ListViewStart
        '
        Me.ListViewStart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListViewStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewStart.HideSelection = False
        Me.ListViewStart.Location = New System.Drawing.Point(8, 6)
        Me.ListViewStart.Name = "ListViewStart"
        Me.ListViewStart.ShowItemToolTips = True
        Me.ListViewStart.Size = New System.Drawing.Size(726, 302)
        Me.ListViewStart.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.ListViewStart.TabIndex = 7
        Me.ListViewStart.UseCompatibleStateImageBehavior = False
        Me.ListViewStart.View = System.Windows.Forms.View.List
        '
        'TabControlStart
        '
        Me.TabControlStart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControlStart.Controls.Add(Me.TabPageStart)
        Me.TabControlStart.Controls.Add(Me.TabPageStop)
        Me.TabControlStart.Controls.Add(Me.TabPageAuswertung)
        Me.TabControlStart.Controls.Add(Me.TabPageUrkunden)
        Me.TabControlStart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlStart.Location = New System.Drawing.Point(0, 0)
        Me.TabControlStart.Name = "TabControlStart"
        Me.TabControlStart.SelectedIndex = 0
        Me.TabControlStart.Size = New System.Drawing.Size(750, 428)
        Me.TabControlStart.TabIndex = 8
        '
        'TabPageStart
        '
        Me.TabPageStart.Controls.Add(Me.Label2)
        Me.TabPageStart.Controls.Add(Me.GroupBox1)
        Me.TabPageStart.Controls.Add(Me.ButtonStart_FixZeit)
        Me.TabPageStart.Controls.Add(Me.ListViewStart)
        Me.TabPageStart.Location = New System.Drawing.Point(4, 29)
        Me.TabPageStart.Name = "TabPageStart"
        Me.TabPageStart.Padding = New System.Windows.Forms.Padding(3, 3, 3, 3)
        Me.TabPageStart.Size = New System.Drawing.Size(742, 395)
        Me.TabPageStart.TabIndex = 0
        Me.TabPageStart.Text = "1 Start"
        Me.TabPageStart.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 313)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(193, 80)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Lauf markieren " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "mehrere mit STRG/SHIFT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "dann Start jetzt drücken" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.DateTimePickerDatumStart)
        Me.GroupBox1.Controls.Add(Me.NumericUpDownMsStart)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ButtonStart_flexZeit)
        Me.GroupBox1.Controls.Add(Me.DateTimePickerZeitStart)
        Me.GroupBox1.Location = New System.Drawing.Point(356, 315)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 80)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Manuelle Zeit"
        '
        'NumericUpDownMsStart
        '
        Me.NumericUpDownMsStart.Location = New System.Drawing.Point(142, 45)
        Me.NumericUpDownMsStart.Maximum = New Decimal(New Integer() {9, 0, 0, 0})
        Me.NumericUpDownMsStart.Name = "NumericUpDownMsStart"
        Me.NumericUpDownMsStart.Size = New System.Drawing.Size(37, 26)
        Me.NumericUpDownMsStart.TabIndex = 9
        Me.NumericUpDownMsStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(128, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 29)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = ","
        '
        'TabPageStop
        '
        Me.TabPageStop.Controls.Add(Me.GroupBox2)
        Me.TabPageStop.Controls.Add(Me.Label3)
        Me.TabPageStop.Controls.Add(Me.ButtonDisquali)
        Me.TabPageStop.Controls.Add(Me.ButtonZeit_Loeschen)
        Me.TabPageStop.Controls.Add(Me.ButtonStopFixZeit)
        Me.TabPageStop.Controls.Add(Me.ListViewStop)
        Me.TabPageStop.Location = New System.Drawing.Point(4, 29)
        Me.TabPageStop.Name = "TabPageStop"
        Me.TabPageStop.Padding = New System.Windows.Forms.Padding(3, 3, 3, 3)
        Me.TabPageStop.Size = New System.Drawing.Size(742, 395)
        Me.TabPageStop.TabIndex = 1
        Me.TabPageStop.Text = "2 Ziel"
        Me.TabPageStop.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.DateTimePickerDatumStop)
        Me.GroupBox2.Controls.Add(Me.NumericUpDownMsStop)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.DateTimePickerZeitStop)
        Me.GroupBox2.Controls.Add(Me.ButtonFlexZeit)
        Me.GroupBox2.Location = New System.Drawing.Point(356, 315)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(376, 80)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Manuelle Zeit"
        '
        'DateTimePickerDatumStop
        '
        Me.DateTimePickerDatumStop.Location = New System.Drawing.Point(6, 19)
        Me.DateTimePickerDatumStop.Name = "DateTimePickerDatumStop"
        Me.DateTimePickerDatumStop.Size = New System.Drawing.Size(242, 26)
        Me.DateTimePickerDatumStop.TabIndex = 12
        '
        'NumericUpDownMsStop
        '
        Me.NumericUpDownMsStop.Location = New System.Drawing.Point(142, 45)
        Me.NumericUpDownMsStop.Maximum = New Decimal(New Integer() {9, 0, 0, 0})
        Me.NumericUpDownMsStop.Name = "NumericUpDownMsStop"
        Me.NumericUpDownMsStop.Size = New System.Drawing.Size(37, 26)
        Me.NumericUpDownMsStop.TabIndex = 13
        Me.NumericUpDownMsStop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(128, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 29)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = ","
        '
        'DateTimePickerZeitStop
        '
        Me.DateTimePickerZeitStop.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.DateTimePickerZeitStop.Location = New System.Drawing.Point(6, 45)
        Me.DateTimePickerZeitStop.MinDate = New Date(2018, 1, 1, 0, 0, 0, 0)
        Me.DateTimePickerZeitStop.Name = "DateTimePickerZeitStop"
        Me.DateTimePickerZeitStop.ShowUpDown = True
        Me.DateTimePickerZeitStop.Size = New System.Drawing.Size(117, 26)
        Me.DateTimePickerZeitStop.TabIndex = 11
        '
        'ButtonFlexZeit
        '
        Me.ButtonFlexZeit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonFlexZeit.Location = New System.Drawing.Point(254, 19)
        Me.ButtonFlexZeit.Name = "ButtonFlexZeit"
        Me.ButtonFlexZeit.Size = New System.Drawing.Size(115, 47)
        Me.ButtonFlexZeit.TabIndex = 13
        Me.ButtonFlexZeit.Text = "Ändere Stopzeit mit eingestellter Zeit"
        Me.ButtonFlexZeit.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(4, 313)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(193, 60)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Läufer markieren " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "mehrere mit STRG/SHIFT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "dann Stop jetzt drücken"
        '
        'ButtonDisquali
        '
        Me.ButtonDisquali.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonDisquali.BackColor = System.Drawing.Color.Red
        Me.ButtonDisquali.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonDisquali.Location = New System.Drawing.Point(292, 313)
        Me.ButtonDisquali.Name = "ButtonDisquali"
        Me.ButtonDisquali.Size = New System.Drawing.Size(59, 34)
        Me.ButtonDisquali.TabIndex = 15
        Me.ButtonDisquali.Text = "Disquali"
        Me.ButtonDisquali.UseVisualStyleBackColor = False
        '
        'ButtonZeit_Loeschen
        '
        Me.ButtonZeit_Loeschen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonZeit_Loeschen.BackColor = System.Drawing.Color.Red
        Me.ButtonZeit_Loeschen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonZeit_Loeschen.Location = New System.Drawing.Point(292, 353)
        Me.ButtonZeit_Loeschen.Name = "ButtonZeit_Loeschen"
        Me.ButtonZeit_Loeschen.Size = New System.Drawing.Size(59, 40)
        Me.ButtonZeit_Loeschen.TabIndex = 10
        Me.ButtonZeit_Loeschen.Text = "Zeit löschen"
        Me.ButtonZeit_Loeschen.UseVisualStyleBackColor = False
        '
        'ButtonStopFixZeit
        '
        Me.ButtonStopFixZeit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonStopFixZeit.BackColor = System.Drawing.Color.Lime
        Me.ButtonStopFixZeit.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonStopFixZeit.Location = New System.Drawing.Point(203, 314)
        Me.ButtonStopFixZeit.Name = "ButtonStopFixZeit"
        Me.ButtonStopFixZeit.Size = New System.Drawing.Size(82, 80)
        Me.ButtonStopFixZeit.TabIndex = 9
        Me.ButtonStopFixZeit.Text = "Stop Jetzt"
        Me.ButtonStopFixZeit.UseVisualStyleBackColor = False
        '
        'ListViewStop
        '
        Me.ListViewStop.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListViewStop.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewStop.HideSelection = False
        Me.ListViewStop.Location = New System.Drawing.Point(8, 6)
        Me.ListViewStop.Name = "ListViewStop"
        Me.ListViewStop.ShowItemToolTips = True
        Me.ListViewStop.Size = New System.Drawing.Size(726, 302)
        Me.ListViewStop.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.ListViewStop.TabIndex = 8
        Me.ListViewStop.UseCompatibleStateImageBehavior = False
        Me.ListViewStop.View = System.Windows.Forms.View.List
        '
        'TabPageAuswertung
        '
        Me.TabPageAuswertung.Controls.Add(Me.ButtonLaufOK)
        Me.TabPageAuswertung.Controls.Add(Me.TextBoxLaufname)
        Me.TabPageAuswertung.Controls.Add(Me.Label6)
        Me.TabPageAuswertung.Controls.Add(Me.Label5)
        Me.TabPageAuswertung.Controls.Add(Me.ButtonAuswertung)
        Me.TabPageAuswertung.Location = New System.Drawing.Point(4, 29)
        Me.TabPageAuswertung.Name = "TabPageAuswertung"
        Me.TabPageAuswertung.Size = New System.Drawing.Size(742, 395)
        Me.TabPageAuswertung.TabIndex = 2
        Me.TabPageAuswertung.Text = "3 Auswertung / Einstellungen"
        Me.TabPageAuswertung.UseVisualStyleBackColor = True
        '
        'ButtonLaufOK
        '
        Me.ButtonLaufOK.Location = New System.Drawing.Point(552, 217)
        Me.ButtonLaufOK.Name = "ButtonLaufOK"
        Me.ButtonLaufOK.Size = New System.Drawing.Size(128, 26)
        Me.ButtonLaufOK.TabIndex = 15
        Me.ButtonLaufOK.Text = "OK"
        Me.ButtonLaufOK.UseVisualStyleBackColor = True
        '
        'TextBoxLaufname
        '
        Me.TextBoxLaufname.Location = New System.Drawing.Point(138, 217)
        Me.TextBoxLaufname.Name = "TextBoxLaufname"
        Me.TextBoxLaufname.Size = New System.Drawing.Size(402, 26)
        Me.TextBoxLaufname.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(11, 220)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 20)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Laufname"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(163, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(286, 60)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Zum Start der Auswertung müssen alle" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Läfuer eine Start und Ende-Zeit haben." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Od" &
    "er Disqualifiziert sein!)"
        '
        'ButtonAuswertung
        '
        Me.ButtonAuswertung.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ButtonAuswertung.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAuswertung.Location = New System.Drawing.Point(3, 3)
        Me.ButtonAuswertung.Name = "ButtonAuswertung"
        Me.ButtonAuswertung.Size = New System.Drawing.Size(154, 59)
        Me.ButtonAuswertung.TabIndex = 0
        Me.ButtonAuswertung.Text = "Starte Auswertung"
        Me.ButtonAuswertung.UseVisualStyleBackColor = False
        '
        'TabPageUrkunden
        '
        Me.TabPageUrkunden.Controls.Add(Me.GroupBoxUrkunden)
        Me.TabPageUrkunden.Location = New System.Drawing.Point(4, 29)
        Me.TabPageUrkunden.Name = "TabPageUrkunden"
        Me.TabPageUrkunden.Padding = New System.Windows.Forms.Padding(3, 3, 3, 3)
        Me.TabPageUrkunden.Size = New System.Drawing.Size(742, 395)
        Me.TabPageUrkunden.TabIndex = 3
        Me.TabPageUrkunden.Text = "4 Urkunden"
        Me.TabPageUrkunden.UseVisualStyleBackColor = True
        '
        'GroupBoxUrkunden
        '
        Me.GroupBoxUrkunden.Controls.Add(Me.CheckBoxTrosturkunde)
        Me.GroupBoxUrkunden.Controls.Add(Me.GroupBoxFilterUrkunde)
        Me.GroupBoxUrkunden.Controls.Add(Me.ButtonUrkundendruck)
        Me.GroupBoxUrkunden.Controls.Add(Me.GroupBoxFilterStufe2Urkunde)
        Me.GroupBoxUrkunden.Enabled = False
        Me.GroupBoxUrkunden.Location = New System.Drawing.Point(8, 11)
        Me.GroupBoxUrkunden.Name = "GroupBoxUrkunden"
        Me.GroupBoxUrkunden.Size = New System.Drawing.Size(648, 359)
        Me.GroupBoxUrkunden.TabIndex = 12
        Me.GroupBoxUrkunden.TabStop = False
        '
        'CheckBoxTrosturkunde
        '
        Me.CheckBoxTrosturkunde.AutoSize = True
        Me.CheckBoxTrosturkunde.Checked = True
        Me.CheckBoxTrosturkunde.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxTrosturkunde.Location = New System.Drawing.Point(103, 275)
        Me.CheckBoxTrosturkunde.Name = "CheckBoxTrosturkunde"
        Me.CheckBoxTrosturkunde.Size = New System.Drawing.Size(319, 24)
        Me.CheckBoxTrosturkunde.TabIndex = 11
        Me.CheckBoxTrosturkunde.Text = "mit Teilehmerurkunde für Disqualifizierte?"
        Me.CheckBoxTrosturkunde.UseVisualStyleBackColor = True
        '
        'GroupBoxFilterUrkunde
        '
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.rB1_alleAK)
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.CheckBoxUrkundeBis)
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.ComboBoxFilterAK)
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.NumericUpDownUrkundeVon)
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.rB1_Statnr_von_b)
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.NumericUpDownUrkundeBis)
        Me.GroupBoxFilterUrkunde.Controls.Add(Me.rB1_folgAK)
        Me.GroupBoxFilterUrkunde.Location = New System.Drawing.Point(6, 14)
        Me.GroupBoxFilterUrkunde.Name = "GroupBoxFilterUrkunde"
        Me.GroupBoxFilterUrkunde.Size = New System.Drawing.Size(616, 133)
        Me.GroupBoxFilterUrkunde.TabIndex = 8
        Me.GroupBoxFilterUrkunde.TabStop = False
        '
        'rB1_alleAK
        '
        Me.rB1_alleAK.AutoSize = True
        Me.rB1_alleAK.Checked = True
        Me.rB1_alleAK.Location = New System.Drawing.Point(17, 25)
        Me.rB1_alleAK.Name = "rB1_alleAK"
        Me.rB1_alleAK.Size = New System.Drawing.Size(152, 24)
        Me.rB1_alleAK.TabIndex = 6
        Me.rB1_alleAK.TabStop = True
        Me.rB1_alleAK.Text = "Alle Altersklassen"
        Me.rB1_alleAK.UseVisualStyleBackColor = True
        '
        'CheckBoxUrkundeBis
        '
        Me.CheckBoxUrkundeBis.AutoSize = True
        Me.CheckBoxUrkundeBis.Location = New System.Drawing.Point(262, 87)
        Me.CheckBoxUrkundeBis.Name = "CheckBoxUrkundeBis"
        Me.CheckBoxUrkundeBis.Size = New System.Drawing.Size(52, 24)
        Me.CheckBoxUrkundeBis.TabIndex = 7
        Me.CheckBoxUrkundeBis.Text = "bis:"
        Me.CheckBoxUrkundeBis.UseVisualStyleBackColor = True
        '
        'ComboBoxFilterAK
        '
        Me.ComboBoxFilterAK.FormattingEnabled = True
        Me.ComboBoxFilterAK.Location = New System.Drawing.Point(225, 54)
        Me.ComboBoxFilterAK.Name = "ComboBoxFilterAK"
        Me.ComboBoxFilterAK.Size = New System.Drawing.Size(385, 28)
        Me.ComboBoxFilterAK.TabIndex = 0
        '
        'NumericUpDownUrkundeVon
        '
        Me.NumericUpDownUrkundeVon.Location = New System.Drawing.Point(180, 85)
        Me.NumericUpDownUrkundeVon.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NumericUpDownUrkundeVon.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownUrkundeVon.Name = "NumericUpDownUrkundeVon"
        Me.NumericUpDownUrkundeVon.Size = New System.Drawing.Size(76, 26)
        Me.NumericUpDownUrkundeVon.TabIndex = 1
        Me.NumericUpDownUrkundeVon.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rB1_Statnr_von_b
        '
        Me.rB1_Statnr_von_b.AutoSize = True
        Me.rB1_Statnr_von_b.Location = New System.Drawing.Point(17, 85)
        Me.rB1_Statnr_von_b.Name = "rB1_Statnr_von_b"
        Me.rB1_Statnr_von_b.Size = New System.Drawing.Size(157, 24)
        Me.rB1_Statnr_von_b.TabIndex = 4
        Me.rB1_Statnr_von_b.Text = "Startnummer von: "
        Me.rB1_Statnr_von_b.UseVisualStyleBackColor = True
        '
        'NumericUpDownUrkundeBis
        '
        Me.NumericUpDownUrkundeBis.Location = New System.Drawing.Point(316, 85)
        Me.NumericUpDownUrkundeBis.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NumericUpDownUrkundeBis.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownUrkundeBis.Name = "NumericUpDownUrkundeBis"
        Me.NumericUpDownUrkundeBis.Size = New System.Drawing.Size(66, 26)
        Me.NumericUpDownUrkundeBis.TabIndex = 2
        Me.NumericUpDownUrkundeBis.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rB1_folgAK
        '
        Me.rB1_folgAK.AutoSize = True
        Me.rB1_folgAK.Location = New System.Drawing.Point(17, 55)
        Me.rB1_folgAK.Name = "rB1_folgAK"
        Me.rB1_folgAK.Size = New System.Drawing.Size(188, 24)
        Me.rB1_folgAK.TabIndex = 3
        Me.rB1_folgAK.Text = "Folgende Altersklasse:"
        Me.rB1_folgAK.UseVisualStyleBackColor = True
        '
        'ButtonUrkundendruck
        '
        Me.ButtonUrkundendruck.Location = New System.Drawing.Point(203, 305)
        Me.ButtonUrkundendruck.Name = "ButtonUrkundendruck"
        Me.ButtonUrkundendruck.Size = New System.Drawing.Size(219, 40)
        Me.ButtonUrkundendruck.TabIndex = 10
        Me.ButtonUrkundendruck.Text = "Druckvorschau / Druck"
        Me.ButtonUrkundendruck.UseVisualStyleBackColor = True
        '
        'GroupBoxFilterStufe2Urkunde
        '
        Me.GroupBoxFilterStufe2Urkunde.Controls.Add(Me.NumericUpDownMaxPlatz)
        Me.GroupBoxFilterStufe2Urkunde.Controls.Add(Me.RB_Limit_Urku)
        Me.GroupBoxFilterStufe2Urkunde.Controls.Add(Me.RadioButton4)
        Me.GroupBoxFilterStufe2Urkunde.Location = New System.Drawing.Point(6, 153)
        Me.GroupBoxFilterStufe2Urkunde.Name = "GroupBoxFilterStufe2Urkunde"
        Me.GroupBoxFilterStufe2Urkunde.Size = New System.Drawing.Size(481, 105)
        Me.GroupBoxFilterStufe2Urkunde.TabIndex = 9
        Me.GroupBoxFilterStufe2Urkunde.TabStop = False
        '
        'NumericUpDownMaxPlatz
        '
        Me.NumericUpDownMaxPlatz.Location = New System.Drawing.Point(95, 57)
        Me.NumericUpDownMaxPlatz.Maximum = New Decimal(New Integer() {30, 0, 0, 0})
        Me.NumericUpDownMaxPlatz.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownMaxPlatz.Name = "NumericUpDownMaxPlatz"
        Me.NumericUpDownMaxPlatz.Size = New System.Drawing.Size(120, 26)
        Me.NumericUpDownMaxPlatz.TabIndex = 2
        Me.NumericUpDownMaxPlatz.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'RB_Limit_Urku
        '
        Me.RB_Limit_Urku.AutoSize = True
        Me.RB_Limit_Urku.Location = New System.Drawing.Point(17, 57)
        Me.RB_Limit_Urku.Name = "RB_Limit_Urku"
        Me.RB_Limit_Urku.Size = New System.Drawing.Size(72, 24)
        Me.RB_Limit_Urku.TabIndex = 1
        Me.RB_Limit_Urku.Text = "ersten"
        Me.RB_Limit_Urku.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Checked = True
        Me.RadioButton4.Location = New System.Drawing.Point(17, 26)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(53, 24)
        Me.RadioButton4.TabIndex = 0
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Alle"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'TimerComport
        '
        '
        'DataGridViewLog
        '
        Me.DataGridViewLog.AllowUserToAddRows = False
        Me.DataGridViewLog.AllowUserToDeleteRows = False
        Me.DataGridViewLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridViewLog.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridViewLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.DataGridViewLog.Location = New System.Drawing.Point(5, 434)
        Me.DataGridViewLog.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.DataGridViewLog.Name = "DataGridViewLog"
        Me.DataGridViewLog.ReadOnly = True
        Me.DataGridViewLog.RowHeadersWidth = 51
        Me.DataGridViewLog.RowTemplate.Height = 24
        Me.DataGridViewLog.Size = New System.Drawing.Size(736, 151)
        Me.DataGridViewLog.TabIndex = 1
        '
        'Column1
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column1.HeaderText = "Level"
        Me.Column1.MinimumWidth = 6
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 58
        '
        'Column2
        '
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column2.HeaderText = "Text"
        Me.Column2.MinimumWidth = 6
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 53
        '
        'Column3
        '
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column3.HeaderText = "Time"
        Me.Column3.MinimumWidth = 6
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 55
        '
        'Column4
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column4.HeaderText = "Modul"
        Me.Column4.MinimumWidth = 6
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 61
        '
        'Pdoc
        '
        '
        'Pprev
        '
        Me.Pprev.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.Pprev.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.Pprev.ClientSize = New System.Drawing.Size(400, 300)
        Me.Pprev.Enabled = True
        Me.Pprev.Icon = CType(resources.GetObject("Pprev.Icon"), System.Drawing.Icon)
        Me.Pprev.Name = "Pprev"
        Me.Pprev.Visible = False
        '
        'Pprintd
        '
        Me.Pprintd.UseEXDialog = True
        '
        'FormStuelpnerlauf
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(750, 595)
        Me.Controls.Add(Me.DataGridViewLog)
        Me.Controls.Add(Me.TabControlStart)
        Me.MinimumSize = New System.Drawing.Size(764, 586)
        Me.Name = "FormStuelpnerlauf"
        Me.Text = "Stülpnerlauf Zeitmessung"
        Me.TabControlStart.ResumeLayout(False)
        Me.TabPageStart.ResumeLayout(False)
        Me.TabPageStart.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUpDownMsStart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageStop.ResumeLayout(False)
        Me.TabPageStop.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.NumericUpDownMsStop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageAuswertung.ResumeLayout(False)
        Me.TabPageAuswertung.PerformLayout()
        Me.TabPageUrkunden.ResumeLayout(False)
        Me.GroupBoxUrkunden.ResumeLayout(False)
        Me.GroupBoxUrkunden.PerformLayout()
        Me.GroupBoxFilterUrkunde.ResumeLayout(False)
        Me.GroupBoxFilterUrkunde.PerformLayout()
        CType(Me.NumericUpDownUrkundeVon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDownUrkundeBis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxFilterStufe2Urkunde.ResumeLayout(False)
        Me.GroupBoxFilterStufe2Urkunde.PerformLayout()
        CType(Me.NumericUpDownMaxPlatz, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridViewLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButtonStart_FixZeit As System.Windows.Forms.Button
    Friend WithEvents DateTimePickerZeitStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePickerDatumStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents ButtonStart_flexZeit As System.Windows.Forms.Button
    Friend WithEvents ListViewStart As System.Windows.Forms.ListView
    Friend WithEvents TabControlStart As System.Windows.Forms.TabControl
    Friend WithEvents TabPageStart As System.Windows.Forms.TabPage
    Friend WithEvents TabPageStop As System.Windows.Forms.TabPage
    Friend WithEvents ListViewStop As System.Windows.Forms.ListView
    Friend WithEvents DateTimePickerZeitStop As System.Windows.Forms.DateTimePicker
    Friend WithEvents ButtonFlexZeit As System.Windows.Forms.Button
    Friend WithEvents DateTimePickerDatumStop As System.Windows.Forms.DateTimePicker
    Friend WithEvents ButtonZeit_Loeschen As System.Windows.Forms.Button
    Friend WithEvents ButtonStopFixZeit As System.Windows.Forms.Button
    Friend WithEvents ButtonDisquali As System.Windows.Forms.Button
    Friend WithEvents NumericUpDownMsStart As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents NumericUpDownMsStop As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabPageAuswertung As System.Windows.Forms.TabPage
    Friend WithEvents ButtonAuswertung As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TimerComport As Timer

    Friend WithEvents DataGridViewLog As DataGridView
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents TabPageUrkunden As TabPage
    Friend WithEvents ButtonUrkundendruck As Button
    Friend WithEvents GroupBoxFilterStufe2Urkunde As GroupBox
    Friend WithEvents NumericUpDownMaxPlatz As NumericUpDown
    Friend WithEvents RB_Limit_Urku As RadioButton
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents GroupBoxFilterUrkunde As GroupBox
    Friend WithEvents rB1_alleAK As RadioButton
    Friend WithEvents CheckBoxUrkundeBis As CheckBox
    Friend WithEvents ComboBoxFilterAK As ComboBox
    Friend WithEvents NumericUpDownUrkundeVon As NumericUpDown
    Friend WithEvents rB1_Statnr_von_b As RadioButton
    Friend WithEvents NumericUpDownUrkundeBis As NumericUpDown
    Friend WithEvents rB1_folgAK As RadioButton
    Friend WithEvents Pdoc As Printing.PrintDocument
    Friend WithEvents Pprev As PrintPreviewDialog
    Friend WithEvents Pprintd As PrintDialog
    Friend WithEvents CheckBoxTrosturkunde As CheckBox
    Friend WithEvents TextBoxLaufname As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents ButtonLaufOK As Button
    Friend WithEvents GroupBoxUrkunden As GroupBox
End Class
