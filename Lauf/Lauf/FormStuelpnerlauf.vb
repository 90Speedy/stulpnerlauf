﻿Imports System.ComponentModel
Imports System.Windows.Forms.AxHost
Imports Newtonsoft
Imports Newtonsoft.Json



Public Class FormStuelpnerlauf
    'Starres Encoding für Projekt --> Festgelegt auf UTF8
    Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
    'Platzhalter für die Datein --> so ca. HTML-Syntax



    Dim mysettings As settings


    'Ordner, die angelegt werden



    ReadOnly settingsdatei As String = "Settings\Settings.txt"
    ReadOnly urkundendatei As String = "Settings\Urkunde.png"


    ReadOnly Ergebnis_vorlage As String = "GesamtErgebnis"
    ReadOnly Ergebnis_nur_erste3_vorlage As String = "Ergebnis_nur_3_Urkunde"



    'Fester String, der eine Diwqualifizierung darstellt im Sinne von nicht angekommen
    ReadOnly Disqualistring As String = "DNF"

    'Formatierung für Datumsangaben
    ReadOnly datumanzeigestring As String = "dd.MM.yyyy HH:mm:ss.f"

    Dim anzeige As New LED_Anzeige()

    Dim laeuferliste As New List(Of laufer)
    Dim startnummer_zu_laufer_liste As New Dictionary(Of Integer, laufer)()
    Dim chip_zu_laufer_liste As New Dictionary(Of ULong, laufer)()


    'alles rings ums drucken
    Dim hilfszaehlerzumdrucken As UInteger = 0
    Dim Urkundenliste As New List(Of laufer)

    Dim comports As New List(Of SerialPort_mitanzeige)







    Sub listen_aktualisieren()
        'Hilfsfunktion zum aktualisieren der Listen
        laufliste_aktualisieren()
        teilnehmerliste_aktualisieren()
    End Sub
    Private Sub initleseLauferinfosausDateien()
        laeuferliste.Clear()
        chip_zu_laufer_liste.Clear()
        startnummer_zu_laufer_liste.Clear()
        System.IO.Directory.CreateDirectory(starterordner)


        For Each file As IO.FileInfo In New System.IO.DirectoryInfo(starterordner).GetFiles("*.txt")
            If IsNumeric(file.Name.Replace(".txt", "")) Then
                Dim mylaufer As New laufer(file.FullName)
                laeuferliste.Add(mylaufer)
                startnummer_zu_laufer_liste.Add(mylaufer.get_nr_as_int, mylaufer)
                Dim chipnr As String = mylaufer.get_chipnr()
                If chipnr <> "" Then
                    If IsNumeric(chipnr) Then
                        If Not (chip_zu_laufer_liste.ContainsKey(CULng(chipnr))) Then
                            chip_zu_laufer_liste.Add(CULng(chipnr), mylaufer)
                        Else
                            Dim vorhander As String = chip_zu_laufer_liste(CULng(chipnr)).get_nr_as_string
                            log_color(errorlevel.Fehler, "Doppelte Chipnummer!: " & mylaufer.get_nr_as_string & " zusätzlich zu: " & vorhander, errortype.EStart, DataGridViewLog)
                        End If
                    Else
                        log_color(errorlevel.Warnung, "Komische Startnummer in: " & mylaufer.get_nr_as_string, errortype.EStart, DataGridViewLog)
                    End If
                Else
                    log_color(errorlevel.Warnung, "Warnung! Keine Chipnummer hinterlegt für Starter: " & mylaufer.get_nr_as_string, errortype.EStart, DataGridViewLog)
                End If
            Else
                log_color(errorlevel.Warnung, "Warnung! Datei gefunden, die nicht passt! " & file.Name, errortype.EStart, DataGridViewLog)
            End If
        Next

        listen_aktualisieren()
    End Sub
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        starte_Comport_Kommunikation()
        anzeige.sende(comports, "Stülpnerlauf", "SV1870 e.V.", "Großolbersdorf", False, False, False, 0)
        System.IO.Directory.CreateDirectory(starterordner)
        loginginit()


        mysettings = New settings(settingsdatei)





        initleseLauferinfosausDateien()




        'Einstellungen holen

        TextBoxLaufname.Text = mysettings.get_Laufname






    End Sub


    Sub teilnehmerliste_aktualisieren()
        'aktualisiert die Liste, indem die Läufer eingetragen sind
        'als Tooltip wird der Name eingetragen
        ListViewStop.Items.Clear()
        For Each mylaufer As laufer In laeuferliste
            Dim tmpitem As ListViewItem = ListViewStop.Items.Add(mylaufer.get_nr_as_string())
            tmpitem.ToolTipText = mylaufer.get_nachname & ", " & mylaufer.get_vorname & " (" & mylaufer.get_lauf & ") " & "| " & mylaufer.get_verein
            If mylaufer.get_stop <> "" Then
                If mylaufer.get_stop = Disqualistring Then
                    'Wemm eine Stopzeit schon eingetragen ist wird die Nummer rot (hinten) gefärbt
                    tmpitem.BackColor = Color.Red
                Else
                    tmpitem.BackColor = Color.DarkOrange
                End If

            End If
            If mylaufer.get_start <> "" Then
                'Wenn der zugehörige Lauf schon eine Startzeit hat, dann wird 
                tmpitem.ForeColor = Color.RoyalBlue
            End If
        Next
    End Sub
    Sub laufliste_aktualisieren()
        'aktualisiert die Läufe
        ListViewStart.Items.Clear()
        ComboBoxFilterAK.Items.Clear()

        For Each mylaufer As laufer In laeuferliste
            'Neuer Lauf wird nur eingetragen, wenn er ne schon in der Liste steht
            Dim tmpitem = ListViewStart.FindItemWithText(mylaufer.get_lauf)
            If tmpitem Is Nothing Then

                tmpitem = ListViewStart.Items.Add(mylaufer.get_lauf)


                ComboBoxFilterAK.Items.Add(mylaufer.get_lauf)
                tmpitem.Tag = 1
                If mylaufer.get_start <> "" Then
                    'gestartete Läufe werden eingefärbt
                    tmpitem.BackColor = Color.Red
                End If
            Else
                tmpitem.Tag += 1

            End If
            tmpitem.ToolTipText = tmpitem.Tag.ToString & " Starter"
        Next
    End Sub


    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles ButtonStart_flexZeit.Click
        'Startzeit wird in die markierten Läufe eingetragen
        trage_startzeit_ein(New Date(
                                        DateTimePickerDatumStart.Value.Year,
                                        DateTimePickerDatumStart.Value.Month,
                                        DateTimePickerDatumStart.Value.Day,
                                        DateTimePickerZeitStart.Value.Hour,
                                        DateTimePickerZeitStart.Value.Minute,
                                        DateTimePickerZeitStart.Value.Second,
                                        (CInt(NumericUpDownMsStart.Value * 100))
                                        ))
        listen_aktualisieren()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles ButtonStart_FixZeit.Click
        'Startzeit wird in die markierten Läufe eingetragen (aber jetzig)
        trage_startzeit_ein(DateTime.Now)
        listen_aktualisieren()
    End Sub

    Sub trage_startzeit_ein(zeit As Date)
        'Startzeit wird in die markierten Läufe eingetragen
        Dim anzahl_der_laufer As UInteger = 0
        Dim selektierte As String = "|"
        For Each item As ListViewItem In ListViewStart.SelectedItems
            selektierte = selektierte & item.Text & "|"
        Next
        If ListViewStart.SelectedItems.Count < 1 Then
            MsgBox("Bitte Läufe markieren, dann Start drücken")
        Else
            For Each item As ListViewItem In ListViewStart.SelectedItems
                ' Dim telegramnachricht As String = "<b>Start: " & item.Text & "</b>" & vbCrLf & "(" & zeit.ToString(datumanzeigestring) & ")" & vbCrLf
                Dim Startnummern As String = vbCrLf & "Startnummern : |"
                For Each mylaufer As laufer In laeuferliste
                    '  If selektierte.Contains("|" & lauf & "|") Then
                    If mylaufer.get_lauf = item.Text Then
                        log_color(errorlevel.OK, mylaufer.get_nr_as_string & " | " & zeit.ToString(datumanzeigestring) & " | " & mylaufer.get_lauf, errortype.EStart, DataGridViewLog)

                        mylaufer.set_start(zeit.ToString(datumanzeigestring))
                        Startnummern = Startnummern & mylaufer.get_nr_as_string & "|"
                        anzahl_der_laufer = (anzahl_der_laufer + CUInt(1))
                    End If

                Next
                Startnummern = Startnummern & vbCrLf
                Dim str_Anzahl_der_starter As String = "Anzahl Starter in der Kategorie: " & anzahl_der_laufer

            Next


        End If





    End Sub
    Sub trage_stopzeit_ein(zeit As Date, datei As String)
        'Hilfsfunkion Datum zu string
        trage_stopzeit_ein(zeit.ToString(datumanzeigestring), datei)
    End Sub


    Sub trage_stopzeit_ein(zeit As String, datei As String)
        'Hilfsfunktion zum Eintragen der Stoppzeit

        log_color(errorlevel.OK, datei & " | " & zeit, errortype.EStop, DataGridViewLog)
        startnummer_zu_laufer_liste(CInt(datei)).set_stop(zeit)
        sucheinfosfuerantwort_anzeige(datei)

    End Sub
    Sub sucheinfosfuerantwort_anzeige(dateiname As String)

        Dim mylaufer As laufer = startnummer_zu_laufer_liste(CInt(dateiname))
        Dim Zeit As String = mylaufer.get_laufzeit_string
        anzeige.sende(comports, mylaufer.get_nr_as_string & " " & mylaufer.get_nachname, mylaufer.get_vorname, Zeit, False, False, True, 20)

    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles ButtonZeit_Loeschen.Click
        'Stopzeit aus läufer löschen
        If MsgBox("Zeit löschen, sicher?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim jetzt As Date = DateTime.Now
            If ListViewStop.SelectedItems.Count > 0 Then
                'Dim selektierte As String = "|"
                For Each item As ListViewItem In ListViewStop.SelectedItems
                    startnummer_zu_laufer_liste(CInt(item.Text)).delete_stop()

                    log_color(errorlevel.Warnung, item.Text & ".txt" & " | " & " Zeit gelöscht", errortype.EStop, DataGridViewLog)

                Next
                listen_aktualisieren()
            Else
                MsgBox("einen Läufer markieren")
            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles ButtonStopFixZeit.Click
        'Stopzeit fix eintragen 
        Dim jetzt As Date = DateTime.Now
        If ListViewStop.SelectedItems.Count > 0 Then
            For Each item As ListViewItem In ListViewStop.SelectedItems
                trage_stopzeit_ein(jetzt, item.Text)
            Next
            listen_aktualisieren()
        Else
            MsgBox("einen Läufer markieren")
        End If
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles ButtonFlexZeit.Click
        'Stopzeit manuell eintragen
        If ListViewStop.SelectedItems.Count > 0 Then
            Dim selektierte As String = "|"
            For Each item As ListViewItem In ListViewStop.SelectedItems
                trage_stopzeit_ein(New Date(
                                   DateTimePickerDatumStop.Value.Year,
                                   DateTimePickerDatumStop.Value.Month,
                                   DateTimePickerDatumStop.Value.Day,
                                   DateTimePickerZeitStop.Value.Hour,
                                   DateTimePickerZeitStop.Value.Minute,
                                   DateTimePickerZeitStop.Value.Second,
                                   CInt(NumericUpDownMsStop.Value * 100)),
                                     item.Text)
            Next
            listen_aktualisieren()
        Else
            MsgBox("einen Läufer markieren")
        End If
        listen_aktualisieren()
    End Sub

    Private Sub ListView2_MouseDoubleClick(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles ListViewStop.MouseDoubleClick
        'Bei Doppeklick kann auch die Stopzeit (jetzt) eingetragen
        If ListViewStop.SelectedItems.Count = 1 Then
            trage_stopzeit_ein(DateTime.Now, ListViewStop.SelectedItems(0).Text)
        End If
        listen_aktualisieren()
    End Sub
    Private Sub ButtonDisquali_Click(sender As System.Object, e As System.EventArgs) Handles ButtonDisquali.Click
        'Disqualifizierung durchführen. Mit nachfragen
        If MsgBox("Disqualifizieren, sicher?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim jetzt As Date = DateTime.Now
            If ListViewStop.SelectedItems.Count > 0 Then

                'Dim selektierte As String = "|"
                For Each item As ListViewItem In ListViewStop.SelectedItems
                    trage_stopzeit_ein(Disqualistring, item.Text)
                Next
                listen_aktualisieren()
            Else
                MsgBox("einen Läufer markieren")
            End If

        End If
    End Sub









    Private Sub Button1_Click_2(sender As System.Object, e As System.EventArgs) Handles ButtonAuswertung.Click
        Dim Ergebnis As String = Ergebnis_vorlage & "_" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".csv"
        Dim Ergebnis_nur_erste3 As String = Ergebnis_nur_erste3_vorlage & "_" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".csv"
        'Auswertung durchführen

        'Listen auf den ersten beiden Seiten aktualisieren, da der Rest darauf aufbaut
        listen_aktualisieren()
        'Vorerst Check, ob es möglich ist
        If check_all_files_if_Start_and_Stoptime_and_lauf_vorhanden() Then
            'Ergebnis löschen
            Try
                If System.IO.File.Exists(Ergebnis) Then
                    System.IO.File.Delete(Ergebnis)
                End If
                If System.IO.File.Exists(Ergebnis_nur_erste3) Then
                    System.IO.File.Delete(Ergebnis_nur_erste3)
                End If
            Catch ex As Exception
                log_color(errorlevel.Fehler, "Kann Ergebnistabellen nicht löschen  --> vielleicht ist sie geöffnet?", errortype.EAuswertung, DataGridViewLog)
                Return
            End Try



            add_zeile_ergebnis("Nr", "Nachname", "Vorname", "Lauf", "Platz", "Gesamtzeit [ms]", "Verein", Ergebnis)
            add_zeile_ergebnis("Nr", "Nachname", "Vorname", "Lauf", "Platz", "Gesamtzeit [ms]", "Verein", Ergebnis_nur_erste3)
            'Jeder Lauf
            For Each Laufitem As ListViewItem In ListViewStart.Items
                'Listen anlegen
                Dim ListLauf As New List(Of laufer)
                Dim ListDISQ As New List(Of laufer)

                'Jeden Starter den Läufen zuordnen
                For Each mylaufer As laufer In laeuferliste
                    If mylaufer.get_lauf = Laufitem.Text Then
                        'Start und Stopinfo lesen

                        'Disqualifizierte in Disqualiliste eintragen
                        If mylaufer.get_stop = Disqualistring Then

                            ListDISQ.Add(mylaufer)
                        Else
                            ListLauf.Add(mylaufer)
                        End If
                    End If
                Next

                'Läufer sortieren
                ListLauf.Sort(Function(x As laufer, y As laufer)
                                  Return x.get_laufzeit.TotalMilliseconds.CompareTo(y.get_laufzeit.TotalMilliseconds)
                              End Function)

                log_color(errorlevel.OK, "Werte Lauf: " & Laufitem.Text, errortype.EAuswertung, DataGridViewLog)

                'Der nächste Abschnitt ist zum Erstellen der Platzierung
                Dim platzierung As Integer = 1
                Dim durchlaufzaehler As Integer = 1
                Dim letztezeit As Double = 0

                For Each mylaufer As laufer In ListLauf
                    If letztezeit = mylaufer.get_laufzeit.TotalMilliseconds Then
                        'Platzierung wird nicht höchgezählt, wenn die Zeit gleich der Vorgängerzeit ist
                    Else
                        'Platzierung hochzählen
                        platzierung = durchlaufzaehler
                    End If
                    log_color(errorlevel.OK, platzierung & " : " & mylaufer.get_nr_as_string & "|" & mylaufer.get_laufzeit.TotalMilliseconds.ToString & "ms", errortype.EAuswertung, DataGridViewLog)



                    'Zeit & Platzierung wird eingetragen

                    hole_alles_andere_aus_datei(mylaufer, platzierung.ToString, mylaufer.get_laufzeit_string, Ergebnis, Ergebnis_nur_erste3)
                    mylaufer.set_platzierung(platzierung)


                    'letzte Zeit wird abgespeichert, um zu sehen ob der nächste Starter auf dem gleichen Platz ist
                    letztezeit = mylaufer.get_laufzeit.TotalMilliseconds
                    durchlaufzaehler += 1
                Next
                For Each mylaufer As laufer In ListDISQ
                    'Disqualifizierte Zeiten noch nachtragen

                    log_color(errorlevel.Hinweis, mylaufer.get_nr_as_string & "|" & Disqualistring, errortype.EAuswertung, DataGridViewLog)


                    hole_alles_andere_aus_datei(mylaufer, Disqualistring, "X", Ergebnis, Ergebnis_nur_erste3)


                Next
                'Trennzeile für Ende-Lauf

                log_color(errorlevel.OK, "-------------------------------------------------", errortype.EAuswertung, DataGridViewLog)

                Me.Refresh()


            Next
            GroupBoxUrkunden.Enabled = True
        End If

    End Sub

    Function check_all_files_if_Start_and_Stoptime_and_lauf_vorhanden() As Boolean
        'Funktion sucht nach möglichen Fehlern für die Auswertung.
        Dim fehlertratauf As Boolean = False
        For Each mylaufer As laufer In laeuferliste


            If mylaufer.get_lauf = "" Then

                log_color(errorlevel.Fehler, "Keine Laufart gefunden in: " & mylaufer.get_nr_as_string, errortype.EAuswertung, DataGridViewLog)


                fehlertratauf = True
            End If
            Dim startzeit As String = mylaufer.get_start
            If startzeit = "" Then

                log_color(errorlevel.Fehler, "Keine Startzeit gefunden in: " & mylaufer.get_nr_as_string, errortype.EAuswertung, DataGridViewLog)


                fehlertratauf = True
            Else
                If Not IsDate(startzeit) Then

                    log_color(errorlevel.Fehler, "Keine Startzeit ist kein Datum in: " & mylaufer.get_nr_as_string, errortype.EAuswertung, DataGridViewLog)


                    fehlertratauf = True
                End If
            End If
            Dim stopzeit As String = mylaufer.get_stop
            If stopzeit = "" Then
                log_color(errorlevel.Fehler, "Keine Stopzeit gefunden in: " & mylaufer.get_nr_as_string, errortype.EAuswertung, DataGridViewLog)
                fehlertratauf = True
            Else
                If Not IsDate(stopzeit) And Not (stopzeit = Disqualistring) Then
                    log_color(errorlevel.Fehler, "Keine Stopzeit ist kein Datum in: " & mylaufer.get_nr_as_string, errortype.EAuswertung, DataGridViewLog)

                    fehlertratauf = True
                End If
            End If
            If startzeit <> "" And stopzeit <> "" And stopzeit <> Disqualistring Then
                If IsDate(startzeit) And IsDate(stopzeit) Then
                    Dim dateStart As Date = DateTime.Parse(startzeit)
                    Dim dateStop As Date = DateTime.Parse(stopzeit)
                    Dim laufzeit As TimeSpan = dateStop - dateStart

                    If laufzeit.TotalMilliseconds < 0 Then
                        log_color(errorlevel.Fehler, "Negative Laufzeit gefunden in: " & mylaufer.get_nr_as_string, errortype.EAuswertung, DataGridViewLog)

                        fehlertratauf = True
                    End If
                End If
            End If
        Next
        If fehlertratauf Then
            Return False
        Else
            Return True
        End If
    End Function
    Sub hole_alles_andere_aus_datei(mylaufer As laufer, platzierung As String, gesamtzeit As String, ergebnispfad As String, ergebnisnurerste3pfad As String)
        add_zeile_ergebnis(mylaufer.get_nr_as_string,
                            mylaufer.get_nachname,
                            mylaufer.get_vorname,
                            mylaufer.get_lauf_without_sortstring,
                            platzierung,
                            gesamtzeit,
                            mylaufer.get_verein,
                            ergebnispfad)


        If IsNumeric(platzierung) Then
            If CInt(platzierung) <= 3 Then 'nur erste 3 für Urkunden
                add_zeile_ergebnis(mylaufer.get_nr_as_string,
                            mylaufer.get_nachname,
                            mylaufer.get_vorname,
                            mylaufer.get_lauf_without_sortstring,
                            platzierung,
                            gesamtzeit,
                            mylaufer.get_verein,
                            ergebnisnurerste3pfad)
            End If
        End If




    End Sub
    Sub add_zeile_ergebnis(startnummer As String, nachname As String, vorname As String, kategorie As String, platzierung As String, gesamtzeit As String, verein As String, ergebnisdatei As String)
        Dim zeile As String = ""
        Dim trennzeichen As String = ";"
        zeile = startnummer & trennzeichen _
                 & nachname & trennzeichen _
                 & vorname & trennzeichen _
                 & kategorie & trennzeichen _
                 & platzierung & trennzeichen _
                 & gesamtzeit & trennzeichen _
                 & verein & vbCrLf

        My.Computer.FileSystem.WriteAllText(ergebnisdatei, zeile, True)
    End Sub





    Sub starte_Comport_Kommunikation()

        tryschliesse_alle_comports(comports)
        comports.Clear()



        Dim Lesegerätportliste As List(Of SerialPort_mitanzeige) = Search_comport_list()
        If Lesegerätportliste.Count > 0 Then

            For Each Lesegerätport In Lesegerätportliste

                log_color(errorlevel.OK, "Lesegerät an " & Lesegerätport.PortName & " gefunden", errortype.EStart, DataGridViewLog)
                Dim port As New SerialPort_mitanzeige(Lesegerätport.PortName, comgeschwindigkeit, IO.Ports.Parity.None, 8)
                port.PortName = Lesegerätport.PortName
                port.AnzeigeAvailable = Lesegerätport.AnzeigeAvailable
                port.Open()
                comports.Add(port)

            Next

            TimerComport.Start()
        Else
            log_color(errorlevel.Fehler, "Kein Leser angeschlossen! Bitte anschließen und Programm neu starten!", errortype.EStart, DataGridViewLog)
        End If






    End Sub

    Private Sub TimerComport_Tick(sender As Object, e As EventArgs) Handles TimerComport.Tick


        Dim fehler As Boolean = False
        For Each port As System.IO.Ports.SerialPort In comports
            Try
                If port.IsOpen() Then
                    If port.BytesToRead > 0 Then
                        Dim tmp As String = port.ReadLine()
                        Dim nummer As ULong
                        nummer = CULng(tmp)

                        If IsNumeric(nummer) Then
                            If chip_zu_laufer_liste.ContainsKey(nummer) Then

                                Dim jetzt As Date = DateTime.Now
                                If chip_zu_laufer_liste(nummer).get_stop() = "" Then
                                    trage_stopzeit_ein(jetzt, chip_zu_laufer_liste(nummer).get_nr_as_string)
                                    listen_aktualisieren()

                                Else

                                    log_color(errorlevel.Warnung, "Zeit schon eingetragen für: " & chip_zu_laufer_liste(nummer).get_nr_as_string & " (" & nummer & ")", errortype.EStop, DataGridViewLog)

                                End If

                                listen_aktualisieren()
                            Else
                                log_color(errorlevel.Warnung, "Warnung! Unbekannte Chipnummer: " & tmp, errortype.EStop, DataGridViewLog)


                            End If
                        Else

                            log_color(errorlevel.Warnung, "Warnung! Lesegerät hat keine Zahl geschickt: " & tmp, errortype.EStop, DataGridViewLog)

                        End If
                    End If
                Else

                    log_color(errorlevel.Fehler, "Fehler! Lesegerätproblem, Stoppe Lesegerät:" & port.PortName, errortype.EStop, DataGridViewLog)

                    fehler = True
                End If
            Catch ex As Exception

                log_color(errorlevel.Fehler, "Fehler! Lesegerätproblem, Stoppe Lesegerät:" & port.PortName, errortype.EStop, DataGridViewLog)

                fehler = True

            End Try

        Next
        If fehler Then
            TimerComport.Stop()
            starte_Comport_Kommunikation()
        End If


    End Sub

    Private Sub ButtonUrkundendruck_Click(sender As Object, e As EventArgs) Handles ButtonUrkundendruck.Click

        Urkundenliste.Clear()



        Dim maxplatz As Integer = 99999
        If RB_Limit_Urku.Checked Then
            maxplatz = NumericUpDownMaxPlatz.Value
        End If
        'alle / mit Filter für AK
        If rB1_alleAK.Checked Or rB1_folgAK.Checked Then
            For Each l As laufer In laeuferliste
                If rB1_alleAK.Checked Or (rB1_folgAK.Checked And ComboBoxFilterAK.Text = l.get_lauf) Then
                    If (Not l.get_stop = Disqualistring) Or (CheckBoxTrosturkunde.Checked) Then
                        If l.get_platzierung <= maxplatz Then
                            Urkundenliste.Add(l)
                        End If
                    End If
                End If
            Next
        End If
        'Filter nach Nummer
        If rB1_Statnr_von_b.Checked Then
            For Each l As laufer In laeuferliste
                If l.get_nr_as_int >= NumericUpDownUrkundeVon.Value Then
                    If l.get_nr_as_int <= NumericUpDownUrkundeBis.Value Then
                        If (Not l.get_stop = Disqualistring) Or (CheckBoxTrosturkunde.Checked) Then
                            Urkundenliste.Add(l)
                        End If

                    End If
                End If
            Next
        End If
        If Urkundenliste.Count > 0 Then


            'Urkundenliste.Sort(Function(x, y) x.age.CompareTo(y.get_platzierung))

            Urkundenliste = Urkundenliste.OrderBy(Function(x) x.get_platzierung).ToList()
            Urkundenliste = Urkundenliste.OrderBy(Function(x) x.get_lauf).ToList()


            Pprev.Document = Pdoc
            If Pprintd.ShowDialog() = vbOK Then
                Pdoc.PrinterSettings = Pprintd.PrinterSettings
                Pprev.ShowDialog()
            End If
        Else
            MsgBox("Keine passenden Läufer zu den eingestellten Filtern gefunden!")
        End If

    End Sub
    Private Sub druckvorschau(sender As Object, e As Printing.PrintPageEventArgs) Handles Pdoc.PrintPage
        If Urkundenliste.Count < 1 Then
            'nichts zu drucken. mal debuggen
        Else


            Dim lauf As String = mysettings.get_Laufname()
            Dim datum As String = Date.Now.ToLongDateString()
            Dim name_SN As String =
                Urkundenliste(hilfszaehlerzumdrucken).get_vorname & " " &
                Urkundenliste(hilfszaehlerzumdrucken).get_nachname & " (" &
                Urkundenliste(hilfszaehlerzumdrucken).get_nr_as_string & ")"
            Dim hatd As String
            Dim platz As String
            Dim PlatzWort As String
            Dim Kateg As String
            Dim Kategauswahl As String
            Dim Zeitwort As String
            Dim ZeitasString As String
            Dim belegt As String

            If Urkundenliste(hilfszaehlerzumdrucken).get_platzierung > 0 Then
                hatd = "hat den"
                platz = Urkundenliste(hilfszaehlerzumdrucken).get_platzierung & "."
                PlatzWort = "Platz"
                Kateg = "in der Kategorie"
                Kategauswahl = Urkundenliste(hilfszaehlerzumdrucken).get_lauf_without_sortstring
                Zeitwort = "in der Zeit"
                ZeitasString = Urkundenliste(hilfszaehlerzumdrucken).get_laufzeit_string
                belegt = "belegt."
            Else
                hatd = ""
                platz = ""
                PlatzWort = "hat"
                Kateg = "in der Kategorie"
                Kategauswahl = Urkundenliste(hilfszaehlerzumdrucken).get_lauf_without_sortstring
                Zeitwort = "teilgenommen."
                ZeitasString = ""
                belegt = ""
            End If






            Dim F32BK As New Font("Arial", 40, FontStyle.Bold Or FontStyle.Italic)
            Dim F14B As New Font("Arial", 25, FontStyle.Bold)
            Dim F10N As New Font("Arial", 16, FontStyle.Regular)
            Dim F10B As New Font("Arial", 16, FontStyle.Bold)



            Dim Mit As Integer = e.PageBounds.Width / 2
            Dim HOF As Integer = e.PageBounds.Height / 2
            Dim B1 As Integer = e.Graphics.MeasureString(hatd, F10N).Width
            Dim B2 As Integer = e.Graphics.MeasureString(platz, F14B).Width
            Dim HDiff As Integer = e.Graphics.MeasureString(platz, F14B).Height - e.Graphics.MeasureString(hatd, F10N).Height - 5 '-5 manuelle Korrektur,.... warum auch immer notwendig,... vll ist auch irgendwo ein fehler
            Dim B3 As Integer = e.Graphics.MeasureString(PlatzWort, F10N).Width
            Dim HGB As Integer = (B1 + B2 + B3) / 2
            If IO.File.Exists(urkundendatei) Then
                Dim bild As Image = Image.FromFile(urkundendatei)
                e.Graphics.DrawImage(bild, 0, 0, Mit * 2, HOF * 2)
            End If
            drawmittig(e, lauf, F32BK, Mit, HOF - 350)
            drawmittig(e, datum, F10N, Mit, HOF - 220)
            drawmittig(e, name_SN, F14B, Mit, HOF - 150)
            e.Graphics.DrawString(hatd, F10N, Brushes.Black, Mit - (HGB), HOF + HDiff - 80)
            e.Graphics.DrawString(platz, F14B, Brushes.Black, Mit - (HGB) + B1, HOF - 80)
            e.Graphics.DrawString(PlatzWort, F10N, Brushes.Black, Mit - (HGB) + B1 + B2, HOF + HDiff - 80)
            drawmittig(e, Kateg, F10N, Mit, HOF - 20)
            drawmittig(e, Kategauswahl, F10B, Mit, HOF + 40)
            drawmittig(e, Zeitwort, F10N, Mit, HOF + 100)
            drawmittig(e, ZeitasString, F10B, Mit, HOF + 160)
            drawmittig(e, belegt, F10N, Mit, HOF + 220)
            hilfszaehlerzumdrucken += 1
            If (Urkundenliste.Count - 1) >= hilfszaehlerzumdrucken Then
                e.HasMorePages = True
            Else
                e.HasMorePages = False
                hilfszaehlerzumdrucken = 0
            End If
        End If

    End Sub
    Sub drawmittig(ByRef e As Printing.PrintPageEventArgs, text As String, font As Font, X As Integer, Y As Integer)
        Dim breite As Integer = e.Graphics.MeasureString(text, font).Width
        e.Graphics.DrawString(text, font, Brushes.Black, X - breite / 2, Y)
    End Sub
    Private Sub rB1_alleAK_CheckedChanged(sender As Object, e As EventArgs) Handles rB1_alleAK.CheckedChanged, rB1_Statnr_von_b.CheckedChanged, rB1_folgAK.CheckedChanged, CheckBoxUrkundeBis.CheckedChanged
        GroupBoxFilterStufe2Urkunde.Visible = Not rB1_Statnr_von_b.Checked
        NumericUpDownUrkundeBis.Visible = CheckBoxUrkundeBis.Checked
        NumericUpDownUrkundeBis.Value = NumericUpDownUrkundeVon.Value
    End Sub

    Private Sub NumericUpDownUrkundeVon_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDownUrkundeVon.ValueChanged
        If NumericUpDownUrkundeVon.Value > NumericUpDownUrkundeBis.Value Or CheckBoxUrkundeBis.Checked = False Then
            NumericUpDownUrkundeBis.Value = NumericUpDownUrkundeVon.Value
        End If
    End Sub
    Private Sub NumericUpDownUrkundeBis_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDownUrkundeBis.ValueChanged
        If NumericUpDownUrkundeVon.Value > NumericUpDownUrkundeBis.Value Then
            NumericUpDownUrkundeVon.Value = NumericUpDownUrkundeBis.Value
        End If
    End Sub
    Private Sub ButtonLaufOK_Click(sender As Object, e As EventArgs) Handles ButtonLaufOK.Click
        mysettings.set_Laufname(TextBoxLaufname.Text)
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)





    End Sub

    Private Sub FormStuelpnerlauf_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        TimerComport.Stop()
        tryschliesse_alle_comports(comports)
    End Sub
End Class