﻿Imports Newtonsoft.Json

Module jsonstarterimport

    Public Class JsonStarter
        Public Property startnummer As String
        Public Property name As String
        Public Property vorname As String
        Public Property chipnummer As String
        Public Property verein As String
        Public Property kategorie As String
        Public Property kategorie_plusSort As String
        Public Property reihenfolge As String
        Public Property geburtsjahr As String
    End Class




    Function getjsonstartest(grid As DataGridView) As List(Of JsonStarter)
        Dim tmpjsonimportedstarters As New List(Of JsonStarter)

        Try
            ' Erstellen des Verzeichnisses


            ' Bereinigen der ListView und der Liste der importierten Starter



            ' URL für den Download
            Dim url As String = "https://stuelpnerlauf.de/wp-content/plugins/wlubb_laufStarterErfassung/sync.php?pw=adfgsdk-rg-gfgsd-23egfgera9sas1_23"

            ' Download der Daten
            Dim thesource As String = New System.Net.WebClient().DownloadString(url)

            ' Deserialisieren der JSON-Daten in eine Liste von JsonStarter-Objekten
            tmpjsonimportedstarters = JsonConvert.DeserializeObject(Of List(Of JsonStarter))(thesource)

            ' Verarbeiten der importierten Starter
            Return tmpjsonimportedstarters

        Catch ex As System.Net.WebException
            log_color(errorlevel.Fehler, "Fehler beim Herunterladen der Daten: " & ex.Message, errortype.EImport, grid)
            Return tmpjsonimportedstarters

        Catch ex As JsonSerializationException
            log_color(errorlevel.Fehler, "Fehler beim Verarbeiten der JSON-Daten: " & ex.Message, errortype.EImport, grid)
            Return tmpjsonimportedstarters

        Catch ex As UnauthorizedAccessException
            log_color(errorlevel.Fehler, "Fehler beim Erstellen des Verzeichnisses: " & ex.Message, errortype.EImport, grid)
            Return tmpjsonimportedstarters

        Catch ex As Exception
            log_color(errorlevel.Fehler, "Ein unerwarteter Fehler ist aufgetreten: " & ex.Message, errortype.EImport, grid)
            Return tmpjsonimportedstarters
        End Try


    End Function
End Module
