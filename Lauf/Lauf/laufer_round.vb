﻿
Public Class laufer_round

    Inherits laufer
    Private rundennummer As Integer
    Private Zrundennummer As String = "Runde"





    Sub New(filepath As String)
        MyBase.New(filepath)
        Dim filecontent As String = My.Computer.FileSystem.ReadAllText(filepath)
        Dim tmp As String = internselectiere_info(filecontent, Zrundennummer)
        If tmp = "" Or (Not (IsNumeric(tmp))) Then
            rundennummer = 0
        Else
            rundennummer = CInt(tmp)
        End If

    End Sub

    Public Function get_runde() As String
        Return (rundennummer)
    End Function

    Public Sub set_runde(myrundennummer As Integer)
        internschreibe_info_in_datei(filepath, rundennummer, Zrundennummer)
        rundennummer = myrundennummer
    End Sub
    Public Sub set_runde_relative(plusminus As Integer)
        rundennummer += plusminus
        internschreibe_info_in_datei(filepath, rundennummer, Zrundennummer)
    End Sub



End Class
