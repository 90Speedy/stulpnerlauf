﻿Imports Microsoft.VisualBasic.Logging
Imports System.Windows.Forms.VisualStyles.VisualStyleElement.Window

Module logging
    ReadOnly logordner As String = "Log"

    'Logdateien
    ReadOnly Startlog As String = "LOG_START.TXT"
    ReadOnly Datalog As String = "LOG_DATA.TXT"
    ReadOnly Stoplog As String = "LOG_STOP.TXT"
    ReadOnly Auswlog As String = "LOG_AUSW.TXT"


    Enum errortype As Integer
        EStart
        EStop
        EAuswertung
        EImport
    End Enum
    Enum errorlevel As Integer
        OK
        Hinweis
        Warnung
        Fehler
    End Enum

    Sub loginginit()
        System.IO.Directory.CreateDirectory(logordner)
    End Sub

    Sub log_color(level As errorlevel, rohtext As String, type As errortype, grid As DataGridView)
        System.IO.Directory.CreateDirectory(logordner)
        Dim myLevel As String = ""
        Dim tmplog As String = ""
        Dim Typestring As String = ""
        Dim Farbe As Color
        If type = errortype.EAuswertung Then
            tmplog = Auswlog
            Typestring = "Auswertung"
        End If
        If type = errortype.EStop Then
            tmplog = Stoplog
            Typestring = "Stop"
        End If
        If type = errortype.EStart Then
            tmplog = Startlog
            Typestring = "Start"
        End If
        If type = errortype.EImport Then
            tmplog = Datalog 'no Textlog
            Typestring = "Datamanagement"
        End If


        If level = errorlevel.OK Then
            myLevel = "OK"
            Farbe = Color.GreenYellow
        End If
        If level = errorlevel.Hinweis Then
            myLevel = "L1"
            Farbe = Color.Yellow
        End If
        If level = errorlevel.Warnung Then
            myLevel = "L2"
            Farbe = Color.Orange
        End If
        If level = errorlevel.Fehler Then
            myLevel = "ER"
            Farbe = Color.Red
        End If


        If grid IsNot Nothing Then
            grid.Rows.Insert(0, myLevel, rohtext, DateTime.Now.ToLongTimeString, Typestring)

            grid.Rows(0).DefaultCellStyle.BackColor = Farbe

            grid.AutoResizeColumn(0)
            grid.AutoResizeColumn(1)
            grid.AutoResizeColumn(2)
            grid.AutoResizeColumn(3)
        Else
            Using fs = New IO.StreamWriter("errorlog.txt", True)
                fs.WriteLine(rohtext)
            End Using
        End If






        If tmplog <> "" Then
            My.Computer.FileSystem.WriteAllText(logordner & "\" & tmplog, DateTime.Now.ToShortTimeString &
           myLevel & ": " & "(" & Typestring & ")" & rohtext & vbCrLf, True)
        End If
    End Sub
End Module
