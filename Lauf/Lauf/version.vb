﻿Module version
    Public PVersion As String = "V1.10"
    'V1.10   Umbau der Lesegeräte auf ESP_Now --> Daten für die Anzeige schickt jetzt das Lesegerät. Kein Router mehr notwendig!
    'V1.00   Datenimport aus Datenbank
    'V0.92   Aktualisierung auf .net 4.8
    'V0.91   Stülpnerlauf Tooltips für Verein + Anzahl der Läufer in der Kategorie eingebaut
    'V0.90   Versionierung eingebaut
End Module
