﻿Imports System.IO.Ports
Imports Microsoft.VisualBasic
Public Module hilfe

    Public Class SerialPort_mitanzeige
        Inherits System.IO.Ports.SerialPort
        Private _anzeigeAvailable As Boolean = False
        Public Property AnzeigeAvailable As Boolean
            Get
                Return _anzeigeAvailable
            End Get
            Set(value As Boolean)
                _anzeigeAvailable = value
            End Set
        End Property
        Public Sub New(portName As String, baudRate As Integer, parity As Parity, dataBits As Integer)
            MyBase.New(portName, baudRate, parity, dataBits)
            _anzeigeAvailable = False
        End Sub

    End Class




    Sub tryschliesse_alle_comports(mycomports As List(Of SerialPort_mitanzeige))
        For Each port As System.IO.Ports.SerialPort In mycomports
            Try
                If port.IsOpen Then
                    port.Close()

                End If
            Catch ex As Exception

            End Try

        Next
    End Sub

    Public Function Search_comport_list() As List(Of SerialPort_mitanzeige)
        Dim liste As New List(Of SerialPort_mitanzeige)

        For Each comp As String In System.IO.Ports.SerialPort.GetPortNames()
            Try
                Dim tmpport As New SerialPort_mitanzeige(comp, comgeschwindigkeit, IO.Ports.Parity.None, 8)
                tmpport.Open()
                Threading.Thread.Sleep(100) 'auf den ersten Mist warten, dass das Lesegerät so schickt
                If tmpport.BytesToRead > 0 Then
                    tmpport.ReadExisting() 'Mist auslesen und verwerfen
                    Threading.Thread.Sleep(100)
                End If
                tmpport.WriteTimeout = 100
                tmpport.WriteLine("IDENT")
                Threading.Thread.Sleep(100) 'auf Antwort warten
                Dim tmp As String = ""
                If tmpport.BytesToRead > 0 Then
                    tmp = tmpport.ReadExisting()
                    tmp = tmp.Replace(vbCr, "")
                    tmp = tmp.Replace(vbLf, "")

                End If
                tmpport.Close()

                If tmp.Contains("Stuelpnerlauf_mit_Anzeige") Then
                    tmpport.AnzeigeAvailable = True
                End If

                If tmp.Contains("Stuelpnerlauf") Then
                    liste.Add(tmpport)
                End If
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
        Next

        Return liste
    End Function
End Module
