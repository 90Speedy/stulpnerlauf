﻿Public Class dateihandling

    Public Function internselectiere_info(roh As String, suche As String) As String
        'sucht aus der Text die gesuchte Info raus .... z.B. den Lauf
        'Wirds ne gefunden wird Leerstring ausgegeben
        Dim anfangsstr As String = "<" & suche & ">"
        Dim endesstr As String = "</" & suche & ">"
        If roh.Contains(anfangsstr) Then
            If roh.Contains(endesstr) Then
                Dim anf As Integer = roh.IndexOf(anfangsstr) + suche.Length + 2 'trennzeichen
                Dim ende As Integer = roh.IndexOf(endesstr) '- 3 ' suche.Length + 4 'trennzeichen von anfang + ende
                Return roh.Substring(anf, ende - anf)
            End If
        End If
        Return ""
    End Function

    Public Sub internschreibe_info_in_datei(filepath As String, zu_schreibende_info As String, Suchstring As String)
        'zentrale Funktion zum eintragen der neuen Information
        'wenn schon drin steht, wird die Info ersetzt, sonst wird sie angehangen
        Dim inhalt As String = ""
        If (System.IO.File.Exists(filepath)) Then
            inhalt = My.Computer.FileSystem.ReadAllText(filepath)
        End If


        Dim anfangsstr As String = "<" & Suchstring & ">"
        Dim endesstr As String = "</" & Suchstring & ">"
        If inhalt.Contains(anfangsstr) Then
            If inhalt.Contains(endesstr) Then
                Dim anf As Integer = inhalt.IndexOf(anfangsstr) + Suchstring.Length + 2 'trennzeichen
                Dim ende As Integer = inhalt.IndexOf(endesstr)  'trennzeichen von anfang + ende
                inhalt = inhalt.Substring(0, anf) & zu_schreibende_info & inhalt.Substring(ende)
                My.Computer.FileSystem.WriteAllText(filepath, inhalt, False)
                Return
            End If
        End If
        My.Computer.FileSystem.WriteAllText(filepath, vbCrLf & "<" & Suchstring & ">" & zu_schreibende_info & "</" & Suchstring & ">", True)
    End Sub

    Public Sub intern_loesche_info_aus_datei(filepath As String, Suchstring As String)
        'löscht die Stopzeit aus der Datei (wird benötigt, wenn man sich verklickt hat)
        Dim inhalt As String = My.Computer.FileSystem.ReadAllText(filepath)
        Dim anfangsstr As String = "<" & Suchstring & ">"
        Dim endesstr As String = "</" & Suchstring & ">"
        If inhalt.Contains(anfangsstr) Then
            If inhalt.Contains(endesstr) Then
                Dim anf As Integer = inhalt.IndexOf(anfangsstr)
                Dim ende As Integer = inhalt.IndexOf(endesstr) + endesstr.Length
                inhalt = inhalt.Remove(anf, ende - anf)
                My.Computer.FileSystem.WriteAllText(filepath, inhalt, False)
                Return
            End If
        End If

    End Sub

End Class
