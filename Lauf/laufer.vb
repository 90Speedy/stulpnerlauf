﻿Imports System.Windows.Forms.VisualStyles.VisualStyleElement

Public Class laufer
    Inherits dateihandling
    Private nr As Integer
    Private platzierung As Integer = 0 'wird nicht abgespeichert
    Private vorname As String
    Private nachname As String

    Private kategorie As String
    Private verein As String
    Protected filepath As String = ""
    Private str_nr As String
    Private startzeit As String
    Private stopzeit As String
    Private chipnr As String
    Private geburtsdatum As String

    Private Zstart As String = "Startzeit"
    Private Zstop As String = "Stoptzeit"
    Private ZVorname As String = "Vorname"
    Private ZNachname As String = "Nachname"
    Private Zlaufart As String = "Kategorie"
    Private ZVerein As String = "Verein"
    Public Const ZChipnr As String = "Chipnr"

    Private filecontent As String


    Public Function get_platzierung() As Integer
        Return (platzierung)
    End Function
    Public Sub set_platzierung(platz As Integer)
        platzierung = platz
    End Sub

    Public Function get_laufzeit() As TimeSpan

        Try
            Dim dateStart As Date = DateTime.Parse(startzeit)
            Dim dateStop As Date = DateTime.Parse(stopzeit)
            Dim laufzeit As TimeSpan = dateStop - dateStart
            If laufzeit.TotalMilliseconds > 0 Then
                Return dateStop - dateStart
            End If


        Catch ex As Exception
        End Try
        Return New TimeSpan(0)


    End Function


    Public Function get_laufzeit_string() As String
        Dim span As TimeSpan = Me.get_laufzeit()

        Return String.Format("{0}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds)
    End Function




    Public Function get_nr_as_string() As String
        Return (str_nr)
    End Function

    Public Function get_nr_as_int() As Integer
        Return (nr)
    End Function
    Public Function get_nachname() As String
        Return (nachname)
    End Function
    Public Function get_vorname() As String
        Return (vorname)
    End Function
    Public Function get_chipnr() As String
        Return (chipnr)
    End Function
    Public Function get_lauf() As String
        Return (kategorie)
    End Function
    Public Function get_lauf_without_sortstring() As String
        If kategorie Like "(##) *" Then
            Return Mid(kategorie, 6)
        Else
            Return (kategorie)
        End If

    End Function

    Public Function get_stop() As String
        Return (stopzeit)
    End Function
    Public Sub delete_stop()
        If filepath <> "" Then
            intern_loesche_info_aus_datei(filepath, Zstop)
        End If

        stopzeit = ""
    End Sub
    Public Sub set_stop(mystopzeit As String)
        If filepath <> "" Then
            internschreibe_info_in_datei(filepath, mystopzeit, Zstop)
        End If

        stopzeit = mystopzeit
    End Sub
    Public Function get_start() As String
        Return (startzeit)
    End Function
    Public Function get_filepath() As String
        Return (filepath)
    End Function
    Public Function get_verein() As String
        Return (verein)
    End Function
    Public Function get_geburtsdatum() As String
        Return (geburtsdatum)
    End Function
    Public Sub set_start(mystartzeit As String)
        If filepath <> "" Then
            internschreibe_info_in_datei(filepath, mystartzeit, Zstart)
        End If

        startzeit = mystartzeit
    End Sub
    Function get_filecontend() As String
        Return filecontent
    End Function
    Sub New(myfilepath As String, vorname As String, nachname As String, kategorie As String, verein As String, chipnr As String)
        'das ist zum anlegen einer datei ,... für inport aus datenbank

        filepath = myfilepath
        str_nr = System.IO.Path.GetFileNameWithoutExtension(filepath)

        internschreibe_info_in_datei(filepath, nachname, ZNachname)
        internschreibe_info_in_datei(filepath, vorname, ZVorname)
        internschreibe_info_in_datei(filepath, kategorie, Zlaufart)
        internschreibe_info_in_datei(filepath, verein, ZVerein)
        internschreibe_info_in_datei(filepath, chipnr, ZChipnr)

        filecontent = My.Computer.FileSystem.ReadAllText(filepath) 'neu einlesen, da auch vorher infos drin gewesen sein könnten
        init(filecontent, str_nr)

    End Sub
    Sub New(myfilepath As String)
        filepath = myfilepath
        str_nr = System.IO.Path.GetFileNameWithoutExtension(filepath)

        filecontent = My.Computer.FileSystem.ReadAllText(filepath)

        init(filecontent, str_nr)

    End Sub
    Sub init(content As String, startnummer As String)
        If IsNumeric(startnummer) Then
            nr = CInt(startnummer)
        Else
            nr = -1
        End If

        vorname = internselectiere_info(content, ZVorname)
        nachname = internselectiere_info(content, ZNachname)
        kategorie = internselectiere_info(content, Zlaufart)
        verein = internselectiere_info(content, ZVerein)
        startzeit = internselectiere_info(content, Zstart)
        stopzeit = internselectiere_info(content, Zstop)
        chipnr = internselectiere_info(content, ZChipnr)



    End Sub
    Sub New(content As String, startnummer As String)
        str_nr = startnummer
        init(content, startnummer)
    End Sub

End Class


