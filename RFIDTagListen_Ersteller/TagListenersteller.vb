﻿Imports System.ComponentModel
Imports System.IO.Ports




Public Class TagListenersteller
    Public maximalzahl_der_Starter = 1000 'von 0 bis 999
    Public Startnummernliste As String = "Chipnummern_Starter.txt"
    Dim Dateihandler As New dateihandling

    Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        SerialPort1.Close()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If SerialPort1.BytesToRead > 0 Then

            Dim indata As String = SerialPort1.ReadExisting()
            indata = indata.Replace(vbCr, "")
            indata = indata.Replace(vbLf, "")
            TextBoxID.Text = indata


            For Each item In ListBox1.Items
                If item.contains("> " & TextBoxID.Text) Then
                    MsgBox("Schlüssel wurde schon folgender Nummer zugewiesen: " & item)
                    Exit Sub
                Else


                End If
            Next
            Dim test As New dateihandling

            Dateihandler.internschreibe_info_in_datei(Startnummernliste, TextBoxID.Text, NumericUpDownStartnummer.Value.ToString("000"))
            Beep()

            NumericUpDownStartnummer.Value = NumericUpDownStartnummer.Value + 1
            inhalt_wieder_aus_datei_lesen()
            ListBox1.SelectedIndex = ListBox1.Items.Count - 1

        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        My.Computer.FileSystem.WriteAllText(Startnummernliste, "", True) 'was leeres anhängen, um sicher zu stellen, dass die datei da ist
        inhalt_wieder_aus_datei_lesen()
        NumericUpDownStartnummer.Maximum = maximalzahl_der_Starter - 1


        Try

            Dim temp As String = Search_comport()
            If temp <> "" Then
                SerialPort1.PortName = Search_comport()
                SerialPort1.Open()
                Timer1.Start()
            Else
                MsgBox("Lesegerät anstecken und neu starten!")
                Me.Close()
                Return
            End If

        Catch ex As Exception
            MsgBox("Fehler!" & ex.Message)
        End Try

    End Sub
    Sub inhalt_wieder_aus_datei_lesen()
        ListBox1.Items.Clear()
        Dim inhalt As String = System.IO.File.ReadAllText(Startnummernliste)
        For i As Integer = 0 To maximalzahl_der_Starter - 1
            Dim i_str As String = i.ToString("000")
            Dim chipnr As String = Dateihandler.internselectiere_info(inhalt, i_str)
            If chipnr <> "" Then
                ListBox1.Items.Add(i_str & " > " & chipnr)
            End If

        Next

    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub
End Class
