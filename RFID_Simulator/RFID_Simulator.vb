﻿Imports System.ComponentModel

Public Class RFID_Simulator
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        TextBox2.Text = ""
        Try

            SerialPort1.Write(NumericUpDown1.Value.ToString)
            SerialPort1.Write(vbCrLf)

            TextBox2.Text = "OK: Nummer wurde gesendet"

        Catch ex As Exception
            TextBox2.Text = "ERR: " & ex.Message
        End Try

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        SerialPort1.PortName = TextBox1.Text
        SerialPort1.Open()
        Timer1.Start()
        Button2.Enabled = False
        Button1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If SerialPort1.BytesToRead > 0 Then
            Dim tmp As String = SerialPort1.ReadLine()
            tmp = tmp.Replace(vbCr, "")
            tmp = tmp.Replace(vbLf, "")
            If tmp = "IDENT" Then
                SerialPort1.Write("Stuelpnerlauf" & vbCrLf)
            End If
        End If
    End Sub



    Private Sub RFID_Simulator_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If SerialPort1.IsOpen Then
            SerialPort1.Close()

        End If
    End Sub
End Class






